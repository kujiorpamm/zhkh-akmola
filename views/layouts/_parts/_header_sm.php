<?php
use yii\helpers\Html;
?>

<div class="wrap header-main">
	<div class="container no-padding">

		<div class="clearfix">
			<div class="pull-left">
				<div class="inline">
					<a href="/">
						<div class="logo">
							<img class="img img-responsive" src="/images/style/logo-akmola.png"/>
						</div>
					</a>

					<div class="logo-text">
						<h1 class="text1"><?=Yii::t('app', 'МОБИЛЬНОЕ УПРАВЛЕНИЕ')?></h1>
						<h1 class="text2"><?=Yii::t('app', 'ЦЕЛИНОГРАДСКОГО РАЙОНА')?></h1>
					</div>
				</div>
			</div>

			<div class="pull-right hidden-xs header-right">
				<div class="inline">
					<div class="phones">
						<span>109</span>
						<span>30772, 30773</span>
						<span>+7 (707) 5630109</span>
					</div>
					<div class="links">
						<a href="/report"><?=Yii::t('app', 'Сообщить о проблеме')?></a>
					</div>
					<div class="div">
						<div class="language">
							<div class="picker kz"><a href="/site/lang?lang=kk">ҚАЗ</a></div>
							<a href="/site/lang"><div class="switch <?=Yii::$app->language?>"></div></a>
							<div class="picker ru"><a href="/site/lang?lang=ru">РУС</a></div>
						</div>
					</div>
				</div>
			</div>

		</div>

		<!--<div class="undp"><img src="/images/style/logo-undp.png"/></div>-->

	</div>
</div>

<?=$this->render('_menu')?>
