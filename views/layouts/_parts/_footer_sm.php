<div class="wrap wrap-bottom">
	<div class="container">

		<div class="row hidden-xs">
			<div class="col-sm-3">

			</div>
			<div class="col-sm-3">
				<div><a href="/report"><?=Yii::t('app', 'Сообщить о проблеме')?></a></div>
				<div><a href="/event-map"><?=Yii::t('app', 'Карта событий')?></a></div>
				<!-- <div><a href="/applications"><?=Yii::t('app', 'Архив событий')?></a></div> -->
				<div><a href="/news"><?=Yii::t('app', 'Архив новостей')?></a></div>
			</div>
			<div class="col-sm-3">
				<!-- <div><a href="/infographics-ouok"><?=Yii::t('app', 'Инфографика: ОУОК')?></a></div> -->
				<!-- <div><a href="/maek-dashboard"><?=Yii::t('app', 'Инфографика: Потребление воды')?></a></div> -->
				<div><a href="/organizations"><?=Yii::t('app', 'Список организаций')?></a></div>
				<div><a href="/stats"><?=Yii::t('app', 'Результаты')?></a></div>
			</div>
			<div class="col-sm-3">
				<div><a href="/login"><?=Yii::t('app', 'Вход в портал')?></a></div>
				<div><a href="/registration"><?=Yii::t('app', 'Регистрация')?></a></div>
				<div><a href="/about"><?=Yii::t('app', 'О портале')?></a></div>
			</div>
		</div>

		<div class="row visible-xs">
			<div class="col-xs-6">
				<div><a href="/report"><?=Yii::t('app', 'Сообщить о проблеме')?></a></div>
				<div><a href="/map"><?=Yii::t('app', 'Карта событий')?></a></div>
				<div><a href="/applications"><?=Yii::t('app', 'Архив событий')?></a></div>
				<div><a href="/news"><?=Yii::t('app', 'Архив новостей')?></a></div>
				<div><a href="/infographics-ouok"><?=Yii::t('app', 'Инфографика: ОУОК')?></a></div>
				<div><a href="/maek-dashboard"><?=Yii::t('app', 'Инфографика: Потребление воды')?></a></div>
			</div>
			<div class="col-xs-6">
				<div><a href="/organizations"><?=Yii::t('app', 'Список организаций')?></a></div>
				<div><a href="/stats"><?=Yii::t('app', 'Результаты')?></a></div>
				<div><a href="/login"><?=Yii::t('app', 'Вход в портал')?></a></div>
				<div><a href="/registration"><?=Yii::t('app', 'Регистрация')?></a></div>
				<div><a href="/about"><?=Yii::t('app', 'О портале')?></a></div>
			</div>
		</div>

	</div>
</div>

<footer class="footer">
    <div class="container">
        <?=Yii::t('app', 'Разработано <a href="http://smart-ex.kz/">SMART-EX</a>')?> &copy;
        <?= date('Y') ?>
    </div>
</footer>
