<?php

namespace app\controllers;

use app\modules\application\models\ApplicationFiles;
use app\modules\user\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

use app\modules\application\models\Application;
use app\modules\application\models\ApplicationAssignment;
use app\modules\application\models\ApplicationTypes;
use app\modules\news\models\News;
use app\modules\organization\models\Addresses;
use app\modules\dashboard\models\Dashboard;
use app\modules\organization\models\OrganizationProjects;
use yii\redactor\RedactorModule;
use kujiorpamm\cropit\widgets\CropitWidget;

class ApiController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'export' => ['post', 'login'],
                ],
            ],
        ];
    }

    public function actionExport($table_name, $date=null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $tables = [
            'application' => Application::find()->select(['id', 'application_type', 'date_created', 'date_plane_end', 'date_end', 'coords', 'seriosness', 'status', 'address', 'last_updated']),
            'application_assignment' => ApplicationAssignment::find()->select(['id', 'application_id', 'organization_id', 'last_updated']),
            'application_types' => ApplicationTypes::find(),
            'organization' => ApplicationTypes::find(),
            'addresses' => Addresses::find(),
        ];

        $headers = apache_request_headers();
        if($_POST['token'] !== "9b1ca857a76266b6fa347ed368db2eb2ad4cc69c7019e6dc7c2c295ac796e107") {
            \Yii::$app->response->setStatusCode(403);
            return ['status' => 403, 'message' => "Доступ запрещен"];
        }

        if(!$tables[$table_name]) {
            \Yii::$app->response->setStatusCode(500);
            return ['status' => 500, 'message' => 'Такой таблицы не существует'];
        }

        $result = ($date) ? $tables[$table_name]->where(['>=', 'last_updated', $date])->all() : $tables[$table_name]->all();
        return $result;
    }

    public function actionLogin() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $email = Yii::$app->request->post('email');
        $password = Yii::$app->request->post('password');

        if(!$email) {
            return [
                'text' => Yii::$app->request->post(),
                'text2' => $_POST
            ];
        }

        if($email && $password) {
            $user = \app\modules\user\models\User::find()->where(['email' => $email])->one();
            if($user && $user->validatePassword($password)) {

                return [
                    'status' => true,
                    'user' => [
                        'id' => $user->id,
                        'fname' => $user->fname,
                        'lname' => $user->lname,
                        'email' => $user->email,
                        'phone' => $user->phone
                    ]
                ];

            } else {
                return [
                    'status' => false,
                    'message' => 'Почта или пароль неправильные'
                ];
            }
        }

        return [
            'text' => 'Не все поля заполнены'
        ];

    }

    public function actionRegistration() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $model = new \app\modules\user\models\User;
        $model->scenario = "registration";

        $_data = Yii::$app->request->post();



        if($model->load($_data, '')) {

            /* Пароль */
            if(!$model->password) {
                return [
                    'status' => false,
                    'message' => 'Необходимо указать пароль'
                ];
            };
            /* Телефон */
            if( !preg_match('/^\\+7_\\d{3}_\\d{7}$/', $model->phone) ) {
                return [
                    'status' => false,
                    'message' => 'Номер телефона указан неправильно'
                ];
            }

            $model->password = md5($model->password);
            $model->authkey = uniqid();
            $model->is_active = '1';
            $model->agreement = '1';

            if($model->validate()) {
                if($model->save()) {
                    $model->assignRole(); //Дать роль пользователю
                    $model->notifyRegistration(); //Уведомить по e-mail о регистрации
                    return [
                        'status' => true,
                        'message' => 'Регистрация завершена',
                        'user' => [
                            'id' => $model->id,
                            'fname' => $model->fname,
                            'lname' => $model->lname,
                            'email' => $model->email,
                            'phone' => $model->phone
                        ]
                    ];
                } else {
                    return [
                        'status' => false,
                        'message' => $this->getFirstError($model->errors),
                    ];
                }
            }
        }
        return [
            'status' => false,
            'message' => $this->getFirstError($model->errors)[0],
        ];
    }

    public function actionGetApplications() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $user_id = Yii::$app->request->post('id');

        if(!$user_id) {
            return [
                'status' => false,
                'message' => 'Ошибка UserId',
                'post' => $_POST
            ];
        }

        $apps = Application::find()->where(['author' => $user_id])->orderBy('date_created DESC')->all();
        $return = [];
        foreach ($apps as $app) {
            $return[] = [
                'id' => $app->id,
                'category' => $app->type->name_ru,
                'subcategory' => $app->subtype->name_ru,
                'date' => $app->date_created,
                'status' => $app->status,
                'status_name' => $app->statusName,
                'image_before' => $app->imageBefore,
            ];
        }

        return [
            'status' => true,
            'data' => $return
        ];
    }

    public function actionGetApplicationData() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $allowed_items = ApplicationTypes::find()->select('name_ru')->indexBy('id')->where(['=', 'parent_id', 1])->column();

        if($allowed_items) {
            return [
                'status' => true,
                'data' => [
                    'types' => $allowed_items
                ]
            ];
        }

        return [
            'status' => false,
            'data' => null,
            'message' => "Ошибка"
        ];
    }

    public function actionGetApplication() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $app_id = Yii::$app->request->post('id');

        if(!$app_id) {
            return [
                'status' => false,
                'message' => 'Ошибка AppId',
                'post' => $_POST
            ];
        }

        $app = Application::find()->where(['id' => $app_id])->one();

        $commends = $app->commends;

        $return = [
            'id' => $app->id,
            'category' => $app->type->name_ru,
            'subcategory' => $app->subtype->name_ru,
            'date' => $app->date_created,
            'status' => $app->status,
            'status_name' => $app->statusName,
            'date_plane_end' => $app->date_plane_end,
            'coords' => $app->coords,
            'organizations' => $app->organizationNames,
            'commends' => [],
            'image_before' => $app->imageBefore,
            'description' => $app->description
        ];

        foreach ($commends as $key => $commend) {
            $return['commends'][] = [
                'author' => $commend->author->fullname,
                'text' => strip_tags($commend->text)
            ];
        }

        return [
            'status' => true,
            'data' => $return
        ];
    }

    public function actionNewApplication() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $user_id = Yii::$app->request->post('user_id');
        $transaction = Yii::$app->db->beginTransaction();

        try {

            $user = \app\modules\user\models\User::find()->where(['id' => $user_id])->one();

            $model = new Application();
            $model->scenario = 'api-create';
            $model->date_created = date('Y-m-d H:i:s');
            $model->date_plane_end = date('Y-m-d H:i:s', strtotime(Yii::$app->params['application']['days_to_add']));
            $model->author = $user_id;
            $model->applicant_name = $user->fname . " " . $user->lname;
            $model->applicant_phone = $user->phone;
            $model->active = 1;
            $model->status = 1;
            $model->application_type = Yii::$app->request->post('category');
            $model->description = Yii::$app->request->post('text');
            $model->image_load = Yii::$app->request->post('image');
            $model->coords = Yii::$app->request->post('coords');

            if ( Yii::$app->request->post('debug') == true ) {

                if($model->validate()) {
                    return [
                        'status' => true
                    ];
                } else {
                    return [
                        'status' => false,
                        'errors' => $model->getErrors(),
                    ];
                }

            }

            if($model->validate() && $model->save()) {
                if($model->image_load) {
                    $filename = "images/applications/" . $model->id . ".png";
                    CropitWidget::saveAs($model->image_load, $filename);
                    $file = new ApplicationFiles;
                    $file->type = 1;
                    $file->ext = 'png';
                    $file->src = "/".$filename;
                    $file->application_id = $model->id;
                    $file->save();

                    $transaction->commit();
                    return [
                        'status' => true,
                        'validate' => $model->validate(),
                        'save' => $model->save(),
                        'filename' => $filename,
                        'errors' => $model->getErrors(),
                    ];
                }
            }
            throw new \Exception("Ошибка, не все поля верные");
        } catch (\Exception $e) {
            $transaction->rollback();
        }

        return [
            'status' => false,
            'errors' => $model->getErrors(),
            'post' => [
                Yii::$app->request->post('category'),
                Yii::$app->request->post('text'),
                Yii::$app->request->post('coords'),
                substr(Yii::$app->request->post('image'), 0, 100),
            ]
        ];
    }

    public function actionMapData() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $data = json_decode(file_get_contents('http://87.255.208.26/json/style.json'));

        $new_layers = [];

        $new_layers = array_map( function ($item, $key) {
            if($item->id == 'background') {
                $item->type = "background";
                $item->{"paint"} = [
                    'background-color' => '#fffef7',
                    'background-opacity' => 0.8,
                ];
            }
            return $item;
        }, $data->layers );

//        $new_layers = $data->layers;
        array_unshift($new_layers, (object) [
            'id' => 'background0',
            'type' => 'raster',
            'source' => 'raster-tiles',
            'paint' => [
                'raster-opacity' => 0.5
            ]
        ]);

        $data->sources->{'raster-tiles'} = [
            'type' => 'raster',
            'tiles' => ["http://a.tile.openstreetmap.org/{z}/{x}/{y}.png", "http://b.tile.openstreetmap.org/{z}/{x}/{y}.png"],
            'tileSize' => 256
        ];
        $data->layers = $new_layers;

        return $this->redirect('/js/style.json');

//        return $data;
    }

    public function beforeAction($action) {
        if (in_array($action->id, ['new-application', 'export', 'login', 'get-applications', 'get-application', 'get-application-data', 'map-data', 'registration'])) {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    private function getFirstError($errors) {
        $keys = array_keys($errors);
        return $errors[$keys[0]];
    }

}
