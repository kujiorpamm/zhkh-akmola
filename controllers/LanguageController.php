<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

use app\models\Message;
use app\models\search\MessageSearch;
use app\models\search\SourceMessageSearch;

class LanguageController extends Controller
{
	
	public $layout = "@app/modules/workplace/views/layouts/main";
	
	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ]                    
                ],
            ]
        ];
    }
    
    
    public function actionIndex()
    {
        $searchModel = new MessageSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);
        Url::remember(\Yii::$app->request->getUrl(), 'i18n-messages-filter');

        $languages = ArrayHelper::map(
            MessageSearch::find()->select('language')->distinct()->all(),
            'language',
            'language'
        );
        $categories = ArrayHelper::map(
            SourceMessageSearch::find()->select('category')->distinct()->all(),
            'category',
            'category'
        );

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'languages' => $languages,
            'categories' => $categories
        ]);
    }
    
    public function actionUpdate($id, $language)
    {
        $model = $this->findModel($id, $language);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(Url::previous('i18n-messages-filter') ?: ['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionSync() {
    	Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$messages = Message::find()->all();
		$file = include("../messages/kk/app.php");
		foreach($file as $rus=>$kaz) {
			
			foreach($messages as $message) {
				if($message->sourceMessage == $rus) {
					if(!$message->translation) {					
						$message->translation = $kaz;
						$message->save();
					}
				}
			}
			
			
		}
		//return $file;
		//return $messages[0]->sourceMessage;
	}
    
    protected function findModel($id, $language)
    {
        if (($model = Message::findOne(['id' => $id, 'language' => $language])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    
}
