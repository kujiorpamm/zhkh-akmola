<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

use app\modules\application\models\Application;
use app\modules\application\models\ApplicationTypes;
use app\modules\news\models\News;
use app\modules\organization\models\Addresses;
use app\modules\dashboard\models\Dashboard;
use app\modules\organization\models\OrganizationProjects;
use yii\redactor\RedactorModule;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionTest() {
		$module = new RedactorModule();
		echo sys_get_temp_dir();
	}

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionLang($lang=null) {
		switch($lang){
			case 'ru':
				Yii::$app->session->set('lang', 'ru');
				break;
			case 'kk':
				Yii::$app->session->set('lang', 'kk');
				break;

			default:

				if(Yii::$app->session->get('lang') == 'ru') {
					Yii::$app->session->set('lang', 'kk');
				} else  if(Yii::$app->session->get('lang') == 'kk') {
					Yii::$app->session->set('lang', 'ru');
				} else {
					Yii::$app->session->set('lang', 'kk');
				}
				break;
		}

		return $this->redirect(Yii::$app->request->referrer);
	}

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        $total = Application::find()->count();
        $solved = Application::find()->where("status = 3")->count();
        $last_completed = Application::getLastTheeCompletedWithImages();
        $last_news = News::find()->where(['category_id'=>'1'])->orderBy(['date'=>SORT_DESC])->limit(4)->all();
        $addresses = Addresses::find()->where(['type'=>1])->select('fullname')->indexBy('id')->column();

        return $this->render('index', [
        	'last_completed'=>$last_completed,
        	'last_news'=>$last_news,
        	'addresses'=>$addresses,
        	'total'=>$total,
        	'solved'=>$solved,
        ]);
    }

    public function actionInfographicsOuok() {
		return $this->render('infographics-ouok');
	}

    public function actionMaekDashboard() {

    	$model = Dashboard::find()->where(['<=', 'date', date('Y-m-d')])->orderBy(['date'=>SORT_DESC])->one();
    	//echo "<pre>"; print_r($model); echo "</pre>"; exit;
		return $this->render('maek-dashboard', ['model'=>$model]);
	}

	public function actionAbout() {
		return $this->render('about');
	}

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionEventMap2() {

		$categores = ApplicationTypes::generateForMenu();

		//Заявки из статуса На проверке / в работе
		//$active_apps = Application::find()->where(['IN', 'status', [1, 2]])->all();

		return $this->render('event-map', ['categores'=>$categores]);
	}

    public function actionEventMap() {

		$categores = ApplicationTypes::generateForMenu();

        $types = ApplicationTypes::find()->where(['IS NOT', 'parent_id', NULL])->all();
        $geojsons = [];
        foreach ($types as $_layer) {
            $objects = Application::find()->where(['application_type'=>$_layer->id])->andWhere(['IN', 'status', [0, 1, 2]])->all();
            $geojson = [
                'id' => $_layer->id,
                'type' => 'FeatureCollection',
                'features' => [],
                'name' => 'layer-' . $_layer->id,
                'icon' => $_layer->icon
            ];
            foreach ($objects as $obj) {
                $coords = explode(',', $obj->coords);
                $geojson['features'][] = [
                    'type' => 'Feature',
                    'geometry' => [
                        'type' => 'Point',
                        'coordinates' => [
                            (float) $coords[1],
                            (float) $coords[0],
                        ]
                    ],
                    'properties' => [
                        // 'title' => $_layer->name_ru,
                        'description' => \yii\helpers\Html::a($_layer->name_ru . " / № {$obj->id}", "/application/{$obj->id}")  . "<br />" . $obj->description
                    ]
                ];
            }
            $geojsons[] = $geojson;
        }

		return $this->render('event-map-new', ['categores'=>$categores, 'geojsons' => $geojsons]);
	}

    public function actionProjectMap2() {

		/*Для блока слева*/
		$active = OrganizationProjects::find()->with('organization')->where(['<=', 'DATE(date_1)', date('Y-m-d')])->andWhere(['>', 'DATE(date_2)', date('Y-m-d')])->all();
		$this_month = OrganizationProjects::find()->where(['>', 'DATE(date_1)', date('Y-m-d')])->andWhere(['<=', 'DATE(date_1)', date('Y-m-t')])->all();
		$other = OrganizationProjects::find()->where(['>', 'DATE(date_1)', date('Y-m-t')])->orderBy(['date_1'=>SORT_ASC])->all();

		/*Для карты*/
		$total_projects = array_merge($active, $this_month);
		foreach($total_projects as $proj) {
			$json_projects[] = [
				'id' => $proj->id,
				'name' => $proj->name,
				'coords' => $proj->coords,
				'date1' => $proj->date_1,
				'date2' => $proj->date_2,
				'descr' => $proj->descr,
				'status' => $proj->status,
				'org_id' => $proj->organization->id,
				'org_name' => $proj->organization->name,
			];
		}
		$json_projects = json_encode($json_projects);

		return $this->render('project-map', ['active'=>$active, 'this_month'=>$this_month, 'other'=>$other, 'json_projects'=>$json_projects]);
	}

    public function actionProjectMap() {

		/*Для блока слева*/
		$active = OrganizationProjects::find()->with('organization')->where(['<=', 'DATE(date_1)', date('Y-m-d')])->andWhere(['>', 'DATE(date_2)', date('Y-m-d')])->all();
		$this_month = OrganizationProjects::find()->where(['>', 'DATE(date_1)', date('Y-m-d')])->andWhere(['<=', 'DATE(date_1)', date('Y-m-t')])->all();
		$other = OrganizationProjects::find()->where(['>', 'DATE(date_1)', date('Y-m-t')])->orderBy(['date_1'=>SORT_ASC])->all();

		/*Для карты*/
		$total_projects = array_merge($active, $this_month);
		foreach($total_projects as $proj) {
			$json_projects[] = [
				'id' => $proj->id,
				'name' => $proj->name,
				'coords' => $proj->coords,
				'date1' => $proj->date_1,
				'date2' => $proj->date_2,
				'descr' => $proj->descr,
				'status' => $proj->status,
				'org_id' => $proj->organization->id,
				'org_name' => $proj->organization->name,
			];
		}
		$json_projects = json_encode($json_projects);

        $objects = $total_projects;
        $geojson = [
            'id' => '1',
            'type' => 'FeatureCollection',
            'features' => [],
            'name' => 'layer-1',
            'icon' => '/images/map-marker.png'
        ];
        foreach ($objects as $obj) {
            $coords = explode(',', $obj->coords);
            $geojson['features'][] = [
                'type' => 'Feature',
                'geometry' => [
                    'type' => 'Point',
                    'coordinates' => [
                        (float) $coords[1],
                        (float) $coords[0],
                    ]
                ],
                'properties' => [
                    'description' => $obj->name_ru . "<br />" . $obj->descr_ru
                ]
            ];
        }

        // echo "<pre>"; var_dump($geojson); echo "</pre>"; exit;

		return $this->render('project-map2', ['active'=>$active, 'this_month'=>$this_month, 'other'=>$other, 'json_projects'=>$json_projects, 'geojsons' => [$geojson]]);
	}

    public function actionPasswordReset() {

        $model = new LoginForm();

        if($model->load(Yii::$app->request->post())) {
            $user = \app\modules\user\models\User::findByUsername($model->username);
            if(!$user) {
                $model->addError('username', Yii::t('app', 'Такой пользователь не найден.'));
            } else {
                $result = $user->resetPasswordFrontend($user);
                if($result) {
                    Yii::$app->session->setFlash('success', 'Ссылка с инструкцией для смены пароля отправлена Вам на электронный адрес: ' . $model->username);
                } else {
                    Yii::$app->session->setFlash('success', 'Ошибка. Обратитесь к администратору.');
                }
                return $this->refresh();
            }
        }

        return $this->render('password-reset', ['model' => $model]);
    }

    public function actionPasswordResetToken($token) {
        $model = \app\modules\user\models\User::findByToken($token);
        if(!$model) throw new \Exception("Вы уже восстанавливали пароль по этой ссылке, либо ее не существует.", 403);

    	$model->scenario = "password-reset";
        $model->password = NULL;

    	if($model->load(Yii::$app->request->post())) {
			$model->password = md5($model->password);
			$model->authkey = uniqid();
			$model->is_active = '1';

			if($model->validate()) {
				if($model->save()) {
					return $this->redirect("/login");
				} else {
					throw new \yii\web\HttpException(404, Yii::t('app', 'Ошибка сохранения, попробуйте еще раз или обратитесь в службу поддержки.'));
				}
			}
		}

		return $this->render('password-reset-token', ['model'=>$model]);

    }

	public function actionJsError() {
		//if(! Yii::$app->request->post()) return false;

		$string = Yii::$app->request->getUserIP();
		$string .= ":	";
		$string .= $_POST['message'];
		$_file = Yii::getAlias("@webroot") . "/js_log.txt";

		if(file_exists($_file)) {
			$this->addToJsLog($string);
		} else {
			$this->createJsLog($_file);
		}
	}

	private function createJsLog($_file) {
		$file = fopen($_file, 'w') or die("Unable to open file!");
		fclose($file);
	}

	private function addToJsLog($string) {
		$file = fopen($_file, 'w') or die("Unable to open file!");
		fwrite($file, $string + '\n');
		fclose($file);
	}

	public function actionLicense() {
        return $this->render('license');
    }

}
