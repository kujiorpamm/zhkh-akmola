
mapboxgl.accessToken = 'pk.eyJ1Ijoia3VqaW9ycGFtbSIsImEiOiJjanF6NGdraDMwOW8yM3hwYzF5Z2RtOGt2In0.8WI8WW9RHOLNEm8yeR7YZQ';
    const map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/kujiorpamm/cjrbhx2w11cda2smn1xelqw1i',
    center: [71.645346, 51.172583],
    zoom: 8.1
});

map.on('load', function () {



});

map.on('click', function(e) {
    $('#mapobjects-coords').val(e.lngLat.lng + ',' + e.lngLat.lat);
    var coordinates = e.lngLat;
    new mapboxgl.Popup()
            .setLngLat(coordinates)
            .setHTML("Тут!")
            .addTo(map);
});
