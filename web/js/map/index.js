$('#map').css( {
    height : $(document).height() + 'px'
});



mapboxgl.accessToken = 'pk.eyJ1Ijoia3VqaW9ycGFtbSIsImEiOiJjanF6NGdraDMwOW8yM3hwYzF5Z2RtOGt2In0.8WI8WW9RHOLNEm8yeR7YZQ';
    const map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/kujiorpamm/cjrbhx2w11cda2smn1xelqw1i',
    center: [71.645346, 51.172583],
    zoom: 8.1
});

map.on('load', function () {

    geojsons.map(function(geojson) {
        map.loadImage(geojson.icon, function(error, image) {
            if (error) throw error;
            map.addImage(geojson.name + 'x', image);
            map.addLayer({
                'id' : geojson.name,
                'type' : 'symbol',
                'source' : {
                    'type' : 'geojson',
                    'data' : geojson
                },
                "minzoom": 11,
                "layout": {
                    "icon-image": geojson.name + 'x',
                    "icon-size": 0.4,
                    "text-field": "{title}",
                    "text-size": 9,
                    "text-offset": [0, 1],
                    "text-anchor": "top"
                }
                // "paint": {
                //     "icon-opacity": {
                //         stops: [[0, 0], [10, 0], [12, 1]]
                //     }
                // }

            });

            // POINTER EVENTS
            map.on('click', geojson.name, function (e) {
                new mapboxgl.Popup()
                    .setLngLat(e.lngLat)
                    .setHTML(e.features[0].properties.description)
                    .addTo(map);
            });
            map.on('mouseenter', geojson.name, function () {
                map.getCanvas().style.cursor = 'pointer';
            });
            map.on('mouseleave', geojson.name, function () {
                map.getCanvas().style.cursor = '';
            });

        });

    });

    // CONTROLS
    $('.layer-control').click(function(e) {
        var clickedLayer = 'layer-' + $(e.target).attr('data-layer');
        e.preventDefault();
        e.stopPropagation();

        var visibility = map.getLayoutProperty(clickedLayer, 'visibility');

        if (visibility === 'visible') {
            map.setLayoutProperty(clickedLayer, 'visibility', 'none');
            this.className = 'layer-control ';
        } else {
            this.className = 'layer-control active';
            map.setLayoutProperty(clickedLayer, 'visibility', 'visible');
        }
    })

});
