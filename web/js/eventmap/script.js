var _vheight = $(window).height() - 177 - 120;
var ymap;
var hidden = [];

var collections = [];

var icon_styles = {
	now: {
		iconLayout: 'default#image',
	    iconImageHref: '/images/style/flag_green.png',
	    iconImageSize: [32, 32],
	    iconImageOffset: [-10, -28]
	},
	this_month: {
		iconLayout: 'default#image',
	    iconImageHref: '/images/style/flag_red.png',
	    iconImageSize: [32, 32],
	    iconImageOffset: [-10, -28]
	}
};

$(document).ready(function(e){

/*
	window.onerror = function eh(errorMsg, url, lineNumber) {
	    $.post('/site/js-error', {message : url + " " + errorMsg + " :: " + lineNumber});
	}*/

	$('.category .name').click(function(){
		$(this).parent().toggleClass('closed');

		window.setTimeout(function(){
			$('.menu').perfectScrollbar('update');
		}, 1000);

	});

	$('.map').attr('height', _vheight + 'px');
	$('.map').attr('style', 'height:' + _vheight + 'px');
	$('.menu').attr('style', 'max-height:' + _vheight + 'px');


	cookies = Cookies.get('hidden');
	if(cookies != undefined) {
		hidden = JSON.parse(Cookies.get('hidden'));
	}



	ymaps.ready(init_view_map);

	/* Будет работать только на странице /event-map */
	if(typeof _categories != 'undefined') {
		ymaps.ready(setApplications);
	}

	/* Будет работать только на странице /event-map */
	if(typeof _items != 'undefined') {
		ymaps.ready(setProjects);
	}


	ymaps.ready(init_elems);

});

function init_elems() {

	$('.item-toggle').click(function() {
		$(this).toggleCollection();
	});
}

function init_view_map() {
	ymap = new ymaps.Map("map", {
        center: [51.149556, 71.411973],
        zoom: 9
    });




	$('.project-item').each(function(e) {
		$(this).mouseenter(function(){
			open_placemarkBalloon($(this).attr('data-attr'));
		});
	});
	$('.project-item').each(function(e) {
		$(this).mouseleave(function(){
			ymap.balloon.close();
		});
	});

	$("#map").mouseleave(function(){
		ymap.balloon.close();
	});

    ymap.geoObjects.events.add(['balloonopen'], function (e) {
    	set_dates();
    });

}

function setProjects() {
	var collection = new ymaps.GeoObjectCollection({ 				// <-- Main
		properties: {
			id: 'projects',
			name: 'projects-collection'
		}
	});

	$(_items).each(function(){
		var app = $(this)[0];
		var coords = app.coords.split(",");
		var pm = new ymaps.Placemark(coords, {
			id: app.id,
			balloonContent:
				'<div class="balloon-project">'+
					'<div class="project-item">'+
						'<div class="heading">'+
							'<span class="icon"></span>'+
							'<span class="date"> <span class="moment-date-base">'+app.date1+'</span> - <span class="moment-date-base">'+app.date2+'</span> </span>'+
							'<div class="text"> <a href="/organizations/'+app.org_id+'">'+app.org_name+' </a> : '+app.name+'</div>'+
						'</div>'+
						'<div class="content">'+app.descr+'</div>'+
					'</div>'+
				'</div>'
			}, icon_styles[app.status]);
		collection.add(pm);
	});

	ymap.geoObjects.add(collection);
}

function setApplications() {

	$(_categories).each(function(){
		//console.log($(this)[0]);
		var catc = $(this)[0];

		collection = new ymaps.GeoObjectCollection({ 				// <-- Main
			properties: {
				id: catc.id,
				name: catc.name
			}
		});

		//console.log(catc.name); Подкатегории
		$(catc.items).each(function() {
			var subcatc = $(this)[0]; 								// <-- Sub
			var subcatc_items = subcatc.apps;
			subcollection = new ymaps.GeoObjectCollection({
				properties: {
					id: subcatc.id,
					name: subcatc.name
				}

			},{
				iconLayout: 'default#image',
				iconImageHref: subcatc.icon, // картинка иконки
				iconImageSize: [30, 30], // размер иконки
			});
			/*
			if(hidden.includes(subcatc.id) == true) {
				subcollection.options.set('visible', !hidden.includes(subcatc.id));
				$('.item-toggle[data-id='+subcatc.id+'] .visibility').addClass('closed');
			} else {
				subcollection.options.set('visible', true);
			}*/

			/*if(hidden.has(subcatc.id) == true) {
				subcollection.options.set('visible', 0);
				$('.item-toggle[data-id='+subcatc.id+'] .visibility').addClass('closed');
			} else {
				subcollection.options.set('visible', true);
			}*/
			if(hidden[subcatc.id] == true) {
				subcollection.options.set('visible', 0);
				$('.item-toggle[data-id='+subcatc.id+'] .visibility').addClass('closed');
			} else {
				subcollection.options.set('visible', true);
			}



			//console.log(subcatc.name);

			$(subcatc_items).each(function(){
				var app = $(this)[0];
				var coords = app.coords.split(",");
				var pm = new ymaps.Placemark(coords, {
					balloonContent:
						'<div class="balloon-c">'+
							'<a href="/application/'+app.id+'">'+subcatc.name+' №'+app.id+'</a>'+
						'</div>'
					}, {});
				subcollection.add(pm);
			});


			collection.add(subcollection);
		});

		collections.push(collection);
		ymap.geoObjects.add(collection);

	});

	$($('.category')[0]).removeClass('closed');
	$('.menu').perfectScrollbar({suppressScrollX: true});

}

function hide_all_marks() {
	ymap.geoObjects.each(function(tt){
		tt.each(function(tt2){
			tt2.options.set('visible', 0);
			$('.visibility').each(function(e){
				$(this).addClass('closed');
			});
		});

	});
	setAllHidden();
}

function show_all_marks() {
	ymap.geoObjects.each(function(tt){
		tt.each(function(tt2){
			tt2.options.set('visible', 1);
			$('.visibility').each(function(e){
				$(this).removeClass('closed');
			});
		});

	});
	setNoHidden();
}

function findCollection(id) {
	var object;
	ymap.geoObjects.each(function(tt){
		if(tt.properties.get('id') == id) object = tt;

		tt.each(function(tt2){
			if(tt2.properties.get('id') == id) object = tt2;
		});

	});
	return object;
}

function findPlacemark(id) {
	var object;
	ymap.geoObjects.each(function(collection){

		collection.each(function(placemark){
			if(placemark.properties.get('id') == id) object = placemark;
		});

	});

	return object;
}

function open_placemarkBalloon(id) {
	var object = findPlacemark(id);
	if( typeof object !== 'undefined') object.balloon.open();
}

$.fn.toggleCollection = function() {

	var current = findCollection($(this).attr('data-id'));
	//console.log(current.options.get('visible'));
	if(current.options.get('visible') || current.options.get('visible') == undefined) {
		current.options.set('visible', 0);
		$(this).find('.visibility').addClass('closed');
		//hidden.push(parseInt($(this).attr('data-id')));
		hidden[$(this).attr('data-id')] = true;
		Cookies.set('hidden', JSON.stringify(hidden));
	} else {
		current.options.set('visible', 1);
		$(this).find('.visibility').removeClass('closed');
		//hidden.pop(parseInt($(this).attr('data-id')));
		//hidden.remove(parseInt($(this).attr('data-id')));
		hidden[$(this).attr('data-id')] = false;
		Cookies.set('hidden', JSON.stringify(hidden));
	}

};

Array.prototype.has = function(val) {
	if(this.indexOf(val) >= 0) return true;
	return false;
};
Array.prototype.save = function() {
	Cookies.set('hidden', JSON.stringify(this));
};
function setAllHidden() {
	//hidden = [];
	ymap.geoObjects.each(function(tt){
		tt.each(function(tt2){
			//hidden.push(tt2.properties.get('id'));
			hidden[tt2.properties.get('id')] = true;
		});
	});
	hidden.save();
};
function setNoHidden() {
	//hidden = [];
	ymap.geoObjects.each(function(tt){
		tt.each(function(tt2){
			//hidden.push(tt2.properties.get('id'));
			hidden[tt2.properties.get('id')] = false;
		});
	});
	hidden.save();
};
