<?php
namespace app\modules\organization\assets;

use yii\web\AssetBundle;

class OrganizationAsset extends AssetBundle {
	
	public $sourcePath = '@organization';
	public $publishOptions = [
	    'forceCopy' => true,
	];
	
	public $css = [
		'css/style.less'
	];
	
	public $depends = [ 
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset', 
        'yii\bootstrap\BootstrapAsset',
        'app\assets\AppAsset', 
    ];  
	
}

?>