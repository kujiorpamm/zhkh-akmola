<?php

namespace app\modules\organization\models;

use Yii;
use app\modules\organization\models\Addresses;
use app\modules\organization\models\OrganizationUsers;
use app\modules\organization\models\OrganizationContract;
use app\modules\organization\models\OrganizationServices;
use app\modules\organization\models\OrganizationProjects;
use app\modules\organization\models\OrganizationReports;
use app\modules\user\models\User;

/**
 * This is the model class for table "organization".
 *
 * @property integer $id
 * @property string $name
 * @property string $activity_ru
 * @property string $activity_kz
 * @property string $heading_ru
 * @property string $heading_kz
 * @property string $rule_ru
 * @property string $rule_kz
 * @property string $contact_ru
 * @property integer $contact_kz
 * @property string $logo
 */
class Organization extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'organization';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'activity_ru', 'contact_ru'], 'required'],
            [['activity_ru', 'activity_kz', 'heading_ru', 'heading_kz', 'rule_ru', 'rule_kz', 'contact_ru', 'contact_kz'], 'string'],
            [['name'], 'string', 'max' => 300],
            [['logo'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Наименование'),
            'activity_ru' => Yii::t('app', 'Деятельность (рус)'),
            'activity_kz' => Yii::t('app', 'Деятельность (каз)'),
            'heading_ru' => Yii::t('app', 'Правление (рус)'),
            'heading_kz' => Yii::t('app', 'Правление (каз)'),
            'rule_ru' => Yii::t('app', 'Устав (рус)'),
            'rule_kz' => Yii::t('app', 'Устав (каз)'),
            'contact_ru' => Yii::t('app', 'Контакты (рус)'),
            'contact_kz' => Yii::t('app', 'Контакты (каз)'),
            'logo' => Yii::t('app', 'Логотип'),
        ];
    }

    public function getContact() {
		return $this->contact_ru;
	}

    public function getActivity() {
		return $this->activity_ru;
	}

    public function getHeading() {
		return $this->heading_ru;
	}

    public function getRule() {
		return $this->rule_ru;
	}

    public function getUserRelations() {
		return $this->hasMany(OrganizationUsers::className, ['organization_id'=>'id']);
	}

	public function getUsers() {
		$models = User::find()->innerJoin('organization_users', 'user.id = organization_users.user_id')
			->andWhere('organization_users.organization_id = "'.$this->id.'"')->all();
		return $models;
	}

  public function getReports() {
    return $this->hasMany(OrganizationReports::className(), ['organization_id'=>'id']);
  }

	public function getContracts() {
		return $this->hasMany(OrganizationContract::className(), ['organization_id'=>'id']);
	}

	public function getAddresses() {
		return $this->hasMany(Addresses::className(), ['organization_id'=>'id']);
	}

	public function getServices() {
		return $this->hasMany(OrganizationServices::className(), ['organization_id'=>'id']);
	}

    public function getProjects() {
		return $this->hasMany(OrganizationProjects::className(), ['organization_id'=>'id']);
	}

}
