<?php

?>

<div class="page-news">

  <div class="wrap">
  	<div class="container">

      <div class="breadcrumbs">
        <span><a href="/"><?=Yii::t('app', 'Главная')?></a></span>
        <span><a href="/organizations"><?=Yii::t('app', 'Организации')?></a></span>
        <span><a href="/organizations/<?=$model->organization->id?>"><?=$model->organization->name?></a></span>
        <span><?=Yii::t('app', 'Отчет - {0}', [Yii::$app->formatter->asDate($model->date, 'php: d.m.Y')])?></span>
      </div>

  		<h2><?=$model->title?></h2>

  		<div class="news-content">
  			<?=$model->text?>

        <div class="file-list">
          <div class="heading <?=$model->files ? : 'hidden'?>"><?=Yii::t('app', 'Файлы')?></div>
          <?php foreach ($model->files as $file) : ?>
            <div class="item">
              <div class="ext"><?=$file->ext?></div>
              <div class="title"><?=$file->title?></div>
              <div class="action"><a target="_blank" href="<?=$file->path?>"><?=Yii::t('app', 'Скачать')?></a></div>
            </div>
          <?php endforeach;?>
        </div>

  		</div>

  	</div>
  </div>

</div>
