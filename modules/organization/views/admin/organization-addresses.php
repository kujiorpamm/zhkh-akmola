<?php

use app\modules\organization\assets\OrganizationAsset;
use yii\helpers\Html;

OrganizationAsset::register($this);

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Организации'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['view-organization', 'id'=>$model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Список адресов на обслуживании');
?>

<div class="workplace">
	
	<div class="heading">
		<h3><?=Yii::t('app', $model->name)?></h3>
		<?=$this->render('_parts/_organization_submenu', ['model'=>$model])?>
		
	</div>
	
	<div class="application-container">
		<?=Html::beginForm()?>

			<div class="row">
				<div class="col-sm-9">
					<?php
						echo \kartik\select2\Select2::widget([
						    'name' => 'address_id',
						    'data' => $addresses,
						    'options' => [
						        'placeholder' => 'Начните вводить адрес',
						        'required' => true
						    ],
						]);
					?>
				</div>
				<div class="col-sm-3">
					<button type="submit" class="btn btn-success btn-block">Добавить дом</button>
				</div>
			</div>

		<?=Html::endForm()?>

		<br/>

		<table class="table">
			<thead>
				<tr>
					<th><?=Yii::t('app', 'Наименование')?></th>
					<th><?=Yii::t('app', '')?></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php
					
					foreach($model->addresses as $address) {
						echo "
							<tr>
								<td>{$address->fullname}</td>
								<td>
									<a href='/organization/admin/unlink-address?id={$address->id}' class='confirm-link'>
										<span class='glyphicon glyphicon-remove'></span>
									</a>
								</td>
							</tr>
						";
					}
					
				?>
			</tbody>
		</table>
	</div>
	
	
</div>


