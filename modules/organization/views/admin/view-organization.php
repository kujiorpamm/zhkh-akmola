<?php

use app\modules\organization\assets\OrganizationAsset;

OrganizationAsset::register($this);

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Организации'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'][] = Yii::t('app', 'Профиль');
?>

<div class="workplace">
	
	<div class="heading">
		<h3><?=Yii::t('app', $model->name)?></h3>
		<?=$this->render('_parts/_organization_submenu', ['model'=>$model])?>		
	</div>
	
	<div class="application-container">
		

		<?=$this->render('_parts/_organization_add', ['model'=>$model])?>
	</div>
	
	
</div>

