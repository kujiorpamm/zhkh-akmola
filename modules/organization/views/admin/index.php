<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\user\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Организации');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="workplace">
	
	<div class="heading">
		
		<div class="clearfix">
			<div class="pull-left"><h3><?= Html::encode($this->title) ?></h3></div>
			<div class="pull-right"><button class="btn btn-success pull-right" onclick="js:$('#m_form').modal('show');"><?=Yii::t('app', 'Добавить организацию')?></button></div>
		</div>
	</div>
	
	<div class="application-container">
		<div class="user-index">

		    <?= GridView::widget([
		        'dataProvider' => $dataProvider,
		        'filterModel' => $searchModel,
		        'columns' => [
		            //['class' => 'yii\grid\SerialColumn'],

		            [
		            	'attribute'=>'id',
		            	'headerOptions' => ['style'=>'width: 80px;']
		            ],
		            'name',
		            [
		            	'class' => 'yii\grid\ActionColumn',
		            	'template' => '{view}',
		            	'urlCreator' => function ($action, $model, $key, $index) {
				            if ($action === 'view') {
				                $url ='/organization/admin/view-organization?id='.$model->id;
				                return $url;
				            }
				        }
		            	
		            ],
		        ],
		    ]); ?>
		</div>
	</div>
	
	
</div>



<?=$this->render('_parts/_organization_add_modal', ['model'=>$model])?>
