<?php
	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\bootstrap\Modal;
	use yii\bootstrap\ActiveForm;
	use kartik\date\DatePicker;
?>
	
	
<?php	
	Modal::begin([
		'id' => 'm_form',
	    'header' => Yii::t('app', 'Добавление пользователя в организацию'),
	    'size' => 'modal-md',
	    'options' => [
	    	'tabindex' => false
	    ],
	    /*'clientOptions'=> [
	    	'show' => true
	    ]*/
	]);
?>
	
	<?php $form = ActiveForm::begin(); ?>
	<?= $form->errorSummary($model); ?>
		
		<?=$form->field($model, 'organization_id')->hiddenInput()->label(false)?>
		<?=$form->field($model, 'name')->textInput(['placeholder'=>$model->getAttributeLabel('name')])->label(false)?>
		<?=$form->field($model, 'description')->textArea(['placeholder'=>$model->getAttributeLabel('description')])->label(false)?>
		<?=$form->field($model, 'email_view')->widget(\kartik\select2\Select2::className(), [
			'data' => Yii::$app->params['email-views'],
			'options' => ['placeholder' => Yii::t('app', 'Выберите шаблон письма')],
		
		])->label(false)?>
		
		<div class="form-group">
		
			<?php
				
				echo DatePicker::widget([
				    'model' => $model,
				    'attribute' => 'start',
				    'attribute2' => 'end',
				    'language' => 'ru',
				    'options' => ['placeholder' => $model->getAttributeLabel('start')],
				    'options2' => ['placeholder' => $model->getAttributeLabel('end')],
				    'type' => DatePicker::TYPE_RANGE,
				    'form' => $form,
				    'pluginOptions' => [
				        'format' => 'dd.mm.yyyy',
				        'autoclose' => true,
				    ],
				    'separator' => 'до'
				]);
				
			?>
			<br/>
		
		</div>
		
		<button type="submit" class="btn btn-success btn-block"> <?=Yii::t('app', 'Добавить')?> </button>
		
	<?php ActiveForm::end(); ?>
	
	
<?php
	Modal::end();
?>
