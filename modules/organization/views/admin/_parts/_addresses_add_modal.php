<?php
	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\bootstrap\Modal;
	use yii\bootstrap\ActiveForm;
	use kartik\date\DatePicker;
?>
	
	
<?php	
	Modal::begin([
		'id' => 'm_form',
	    'header' => Yii::t('app', 'Добавление адреса'),
	    'size' => 'modal-md',
	    'options' => [
	    	'tabindex' => false
	    ],
	    /*'clientOptions'=> [
	    	'show' => true
	    ]*/
	]);
?>
	
	<p>Нужно указать является ли это микрорайоном или это здание (дом), <br/>
	указать его название в формате НомерБуква_мкр или НомерБуква_д <br/>
	Например "<b>12 мкр</b>" - для микрорайона или "<b>11 д</b>" - для дома <br/>
	<b>Это важно</b>
	</p><br/>
	
	<?php $form = ActiveForm::begin(); ?>
	<?= $form->errorSummary($model); ?>
		
		<?=$form->field($model, 'parent_id')->hiddenInput()->label(false)?>
		<?=$form->field($model, 'type')->dropDownList(['0'=>'Район', '1'=>'Дом'])->label(false)?>
		<?=$form->field($model, 'name')->textInput(['placeholder'=>$model->getAttributeLabel('name')])->label(false)?>
		
		
		
		<button type="submit" class="btn btn-success btn-block"> <?=Yii::t('app', 'Добавить')?> </button>
		
	<?php ActiveForm::end(); ?>
	
	
<?php
	Modal::end();
?>
