<?php

?>

<div class="controller">

	<div class="btn-group">
		<a data-attr="view-organization" class="btn btn-link as-link act" href="/organization/admin/view-organization?id=<?=$model->id?>"><?=Yii::t('app', 'Профиль')?></a>
		<a data-attr="organization-contracts" class="btn btn-link as-link act" href="/organization/admin/organization-contracts?id=<?=$model->id?>"><?=Yii::t('app', 'Контракты')?></a>
		<a data-attr="organization-users" class="btn btn-link as-link act" href="/organization/admin/organization-users?id=<?=$model->id?>"><?=Yii::t('app', 'Пользователи')?></a>
		<a data-attr="organization-addresses" class="btn btn-link as-link act" href="/organization/admin/organization-addresses?id=<?=$model->id?>"><?=Yii::t('app', 'Дома')?></a>
		<a data-attr="organization-services" class="btn btn-link as-link act" href="/organization/admin/organization-services?id=<?=$model->id?>"><?=Yii::t('app', 'Услуги')?></a>
	</div>

</div>

<?php
	$this->registerJs("
	
		$('a[data-attr=\"".Yii::$app->controller->action->id."\"]').addClass('active');
	
	");
?>