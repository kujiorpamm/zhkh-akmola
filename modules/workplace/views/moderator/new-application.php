<?php
	$this->title = $model->id;
	$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Рабочий стол'), 'url' => ['/workplace']];
	$this->params['breadcrumbs'][] = Yii::t('app', 'Новая заявка');
	widgets\mapbox\MapboxWidget::widget(['page'=>'report']);
?>

<div class="workplace">

	<div class="heading">
		<h3><?=Yii::t('app', 'Добавление заявки')?></h3>

	</div>

	<div class="application-container">
		<?=$this->render("_parts/_app_form", ['model'=>$model,
										'category_items'=>$category_items,
										'subcategory_items'=>$subcategory_items,
										'is_editing' => false])?>
	</div>


</div>
