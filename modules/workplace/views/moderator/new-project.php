<?php
	use yii\bootstrap\ActiveForm;
	use yii\bootstrap\Html;
	use kartik\datetime\DateTimePicker;
	use app\modules\application\assets\ApplicationCreateAsset;
	ApplicationCreateAsset::register($this);
	

	$this->title = $model->id;
	$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Рабочий стол'), 'url' => ['/workplace']];
	$this->params['breadcrumbs'][] = Yii::t('app', 'Новый проект');
	
	
	
?>

<div class="workplace">
	
	<div class="heading">
		<h3><?=Yii::t('app', 'Добавление проекта')?></h3>
		
	</div>
	
	<div class="application-container">
		
		<?php $form = ActiveForm::begin(['id'=>'app_form']); ?>
		<div class="row">
			<!--ЛЕВАЯ КОЛОНКА-->
			<div class="col-md-6 col-sm-8 col-xs-12">		
				<div class="form-splitter"><?=Yii::t('app', 'Общие данные')?></div>
				<div>
					<!--Наименование-->
					<div class="form-group">
						<?=$form->field($model, 'name_ru')->textInput(['placeholder'=>Yii::t('app', 'Имя проекта (рус.)')])->label(false)?>
					</div>					
					<!--Наименование-->
					<div>
						<?=$form->field($model, 'name_kz')->textInput(['placeholder'=>Yii::t('app', 'Имя проекта (каз.)')])->label(false)?>
					</div>
					
					<!--Описание-->
					<div class="form-group">
						<?=$form->field($model, 'descr_ru')->textArea(['placeholder'=>Yii::t('app', 'Описание (рус.)')])->label(false)?>
					</div>
					<!--Описание-->
					<div class="form-group">
						<?=$form->field($model, 'descr_kz')->textArea(['placeholder'=>Yii::t('app', 'Описание (каз.)')])->label(false)?>
					</div>
					<br />
					<!--Даты-->
					<div class="row">
						<div class="col-sm-6">
							<!--Дата создания-->
							<div class="form-group">
								<label><?=Yii::t('app', 'Дата <br/> начала проекта')?> <span class="note glyphicon glyphicon-question-sign"><div>Дата создания заявки (по умолчанию - сегодня)</div></span></label>
								<?=$form->field($model, 'date_1')->widget(DateTimePicker::classname(), [
								    'options' => ['placeholder' => Yii::t('app', 'Дата создания')],
								    'pluginOptions' => [
										'todayHighlight' => true,
										'todayBtn' => true,
										'format' => 'dd.mm.yyyy hh:ii:ss',
										'autoclose' => true,
									]
								])->label(false)?>
							</div>
						</div>
						<div class="col-sm-6">
							<!--Дата планируемого завершения-->
							<div class="form-group">
								<label><?=Yii::t('app', 'Дата  <br/>планируемого завершения')?> <span class="note glyphicon glyphicon-question-sign"><div>Дата планируемого завершения заявки (по умолчанию - плюс один день)</div></span> </label>
								<?=$form->field($model, 'date_2')->widget(DateTimePicker::classname(), [
								    'options' => ['placeholder' => Yii::t('app', 'Дата планируемого завершения')],
								    'pluginOptions' => [
										'todayHighlight' => true,
										'todayBtn' => true,
										'format' => 'dd.mm.yyyy hh:ii:ss',
										'autoclose' => true,
									]
								])->label(false)?>
							</div>
						</div>
					</div>					
				</div>
			</div>
			<!--КОНЕЦ ЛЕВАЯ КОЛОНКА-->
			
			<!--ПРАВАЯ КОЛОНКА-->
			<div class="col-md-6 col-sm-4 col-xs-12">
				
				<div class="form-splitter"><label for="OrganizationProjects[coords]"><?=Yii::t('app', 'Координаты')?></label></div>
				<div class="same-height">
					<!--Метка на карте-->
					<div>
						<?=$form->field($model, 'coords')->hiddenInput(['class'=>'controller-coords'])->label(false)?>
					</div>
					
					<div class="yandex-map" id="ymap">
						
					</div>
				</div>
				
			</div>
			<!--КОНЕЦ ПРАВАЯ КОЛОНКА-->
			
			
		</div>
		
		<br />
		<br />
		
		<div class="clearfix">
			<div class="pull-right">
				<button type="submit" class="btn btn-lg btn-success"> <?=Yii::t('app', 'Добавить проект')?> </button>
			</div>
		</div>
		
		<?php ActiveForm::end(); ?>
	</div>
	
	
	
	
</div>