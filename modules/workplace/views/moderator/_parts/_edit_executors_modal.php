<?php
	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\bootstrap\Modal;
	use yii\bootstrap\ActiveForm;
	use kartik\select2\Select2;
?>

<?php	
	Modal::begin([
		'id' => 'm_form_executors',
	    'header' => Yii::t('app', 'Назначение исполнителя'),
	    'size' => 'modal-md',
	    'options' => [
	    	'tabindex' => false
	    ]/*,
	    'clientOptions'=> [
	    	'show' => true
	    ]*/
	]);
?>
	
	<?php $form = ActiveForm::begin(['action'=>'/workplace/moderator/assign-organization']); ?>
		<div class="form-group">
			
			<?=$form->field($model, 'application_id')->hiddenInput()->label(false)?>
			<?=$form->field($model, 'application_end')->hiddenInput()->label(false)?>
			<?= $form->field($model, 'organization_id')->widget(Select2::classname(), [
			    'data' => $executors,
			    'options' => ['placeholder' => Yii::t('app', 'Выберите исполнителя')],
			    'pluginOptions' => [
			        'allowClear' => false
			    ],
			])->label(false); ?>
			
			<div class="application_contract_id_container" style="display: none;">			
				<label><?=Yii::t('app', 'Контракт')?> <span class="note glyphicon glyphicon-question-sign"><div>Контракт можно не указывать</div></span></label>
				<?= $form->field($model, 'contract_id')->dropDownList(null, ['prompt'=>Yii::t('app', 'Выберите контракт')])->label(false)?>
			</div>
		</div>
		
		
		<button type="submit" class="btn btn-success btn-block"> <?=Yii::t('app', 'Добавить')?> </button>
		
	<?php ActiveForm::end(); ?>
	
	
<?php
	Modal::end();
?>

<?=$this->registerJs("

	

")?>