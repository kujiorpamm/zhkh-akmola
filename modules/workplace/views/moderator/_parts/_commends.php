<?php
	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\bootstrap\ActiveForm;
	use kujiorpamm\cropit\widgets\CropitWidget;
?>

<div class="commend-form">

<?php $form = ActiveForm::begin(['action'=>'/workplace/moderator/add-commend']); ?>
<?= $form->errorSummary($model); ?>
	
	<div class="form-group">
		<?= $form->field($model, 'application_id')->hiddenInput()->label(false)?>
		<?= $form->field($model, 'text')->widget(\yii\redactor\widgets\Redactor::className(), [
			'clientOptions' => [
				'plugins' => ['imagemanager','filemanager', 'video'],
				'minHeight' => 500,
				'lang' => 'ru',
				'minHeight' => '300px',
				'buttons' => ['link', 'video', 'image', 'file'],
				'placeholder' => Yii::t('app', 'Ваш комментарий')
			]
		])->label(false);?>
		
	</div>
	
		
	<div class="clearfix">
		<div class="pull-right">
			<button type="submit" class="btn btn-success"> <?=Yii::t('app', 'Отправить комментарий')?> </button>
		</div>
	</div>
	
<?php ActiveForm::end(); ?>

</div>
