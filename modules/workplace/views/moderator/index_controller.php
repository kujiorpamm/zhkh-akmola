<?php
	
	$this->title = "Рабочий стол";
	$this->params['active'] = $active;
	
?>

<?=$this->render('@app/modules/workplace/views/layouts/_parts/_workplace', [
	'active'=>$active, 
	'active_count'=>$active_count,
	'solved_count'=>$solved_count,
	'new_count'=>$new_count,
	'work_count'=>$work_count,
	'frozen_count'=>$frozen_count,
	'types' => $types
])?>