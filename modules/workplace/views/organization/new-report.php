<?php
	use yii\bootstrap\ActiveForm;
	use yii\bootstrap\Html;
	$this->title = Yii::t('app', 'Новый отчет');
	$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Рабочий стол'), 'url' => ['/workplace']];
	$this->params['breadcrumbs'][] = Yii::t('app', 'Новый отчет');
?>

<div class="workplace">

	<div class="heading">
		<h3><?=Yii::t('app', 'Добавление отчета')?></h3>
	</div>

	<div class="application-container">

    <?=$this->render('_form_report', ['model'=>$model])?>

	</div>




</div>
