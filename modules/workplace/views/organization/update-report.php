<?php
	use yii\bootstrap\Modal;
	use yii\bootstrap\ActiveForm;
	use yii\bootstrap\Html;
	$this->title = Yii::t('app', 'Редактирование отчета');
	$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Рабочий стол'), 'url' => ['/workplace']];
	$this->params['breadcrumbs'][] = $this->title;



?>

<div class="workplace" id="update-report">

	<div class="heading"><h3><?=$this->title?></h3></div>

	<div class="application-container">

  	<div class="row">
			<div class="col-sm-6">
				<?=$this->render('_form_report', ['model'=>$report])?>
			</div>
			<div class="col-sm-6">
				<div class="clearfix">
					<div class="pull-left"><h3>Список файлов: </h3></div>
					<div class="pull-right"><button class="btn btn-success" onclick="$('#file_modal').modal('show')">  <i class="glyphicon glyphicon-plus"></i>  Добавить файл</button></div>
				</div>
				<div id="file-list">
					<div class="file-list">
						<?php foreach ($report->files as $_file) : ?>
							<div class="item">
								<div class="ext"><?=$_file->ext?></div>
								<div class="title"><?=$_file->title?></div>
								<div class="action"><a class="confirm-link" href='/workplace/organization/delete-file?id=<?=$_file->id?>'>Удалить</a></div>
							</div>
						<?php endforeach;?>
					</div>
		    </div>
			</div>
		</div>





	</div>

</div>

<?php Modal::begin([
    'header' => 'Прикрепить файл к отчету',
    'id' => 'file_modal',
]);?>

<div id="file-form">
	<?php $form_file = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
		<?=$form_file->field($file, 'title_ru')->textInput(['placeholder'=>Yii::t('app', 'Имя файла (рус)')])->label(false)?>
			<?=$form_file->field($file, 'title_kz')->textInput(['placeholder'=>Yii::t('app', 'Имя файла (каз)')])->label(false)?>
			<?=$form_file->field($file, 'file')->fileInput()->label(false)?>
			<button class="btn btn-md btn-block btn-success">Добавить</button>
		</div>
	<?php ActiveForm::end(); ?>
</div>

<?php Modal::end();?>
