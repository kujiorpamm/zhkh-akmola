<?php

namespace app\modules\workplace\controllers;

use yii\web\Controller;

/**
 * Default controller for the `workplace` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        // admin and modertor
        if(\Yii::$app->user->can('admin') || \Yii::$app->user->can('moderator')) {
			return $this->redirect("/workplace/moderator");
		}
		
		// organization
        if(\Yii::$app->user->can('organization')) {
			return $this->redirect("/workplace/organization");
		}
    }
    
    public function actionArchive() {
		
		// admin and modertor
        if(\Yii::$app->user->can('admin') || \Yii::$app->user->can('moderator')) {
			return $this->redirect("/workplace/moderator/archive");
		}
		
		// organization
        if(\Yii::$app->user->can('organization')) {
			return $this->redirect("/workplace/organization/archive");
		}
	}
    
    public function actionView($id) {
		
		// admin and modertor
        if(\Yii::$app->user->can('admin') || \Yii::$app->user->can('moderator')) {
			return $this->redirect("/workplace/moderator/view?id={$id}");
		}
		
		// organization
        if(\Yii::$app->user->can('organization')) {
			return $this->redirect("/workplace/organization/view?id={$id}");
		}
	}
}
