<?php

namespace app\modules\workplace;

/**
 * workplace module definition class
 */
class module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\workplace\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
		$this->setAliases([ 
            '@workplace' => __DIR__ 
        ]);
        // custom initialization code goes here
    }
}
