<?php

namespace app\modules\user\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use app\modules\user\models\User;
use app\modules\user\models\UserSearch;

/**
 * Default controller for the `user` module
 */
class AdminController extends Controller
{
    
    public $layout = "@app/modules/workplace/views/layouts/main";
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin', 'moderator'],
                    ]
                    
                ],
            ]
        ];
    }
    
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionListUsers() {
		return $this->render('list-users');
	}
	
	public function actionViewUser($id) {
		$model = $this->findModel($id);
		
		$model->scenario = "admin-update";
		$model->role = $model->getRole();
		
		$possible_roles = (new \yii\db\Query())
			->select('description')
		    ->from('auth_item')
		    ->indexBy('name')->column();
		
		if($model->load(Yii::$app->request->post())) {			
			$model->assignRole($model->role);
			if($model->save()) {
				return $this->refresh();
			} else {
				//return $this->render('registration', ['model'=>$model]);
				throw new \yii\web\HttpException(404, Yii::t('app', 'Ошибка сохранения, попробуйте еще раз или обратитесь в службу поддержки.'));
			}
		}
		
		return $this->render('view-user', [
            'model' => $model,
            'roles' => $possible_roles
        ]);
	}
	
	public function actionResetPassword() {
		$model = $this->findModel($_POST['id']);
		if($model->resetPassword()) {
			echo json_encode(['status'=>1, 'message'=>Yii::t('app', 'Пароль успешно изменен. Новый пароль отправлен на почту пользователя.')]);
		} else {
			echo json_encode(['status'=>0, 'message'=>Yii::t('app', 'Ошибка. Попробуйте еще раз или обратитесь в службу поддержки.')]);
		}
	}
	
	protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
}
