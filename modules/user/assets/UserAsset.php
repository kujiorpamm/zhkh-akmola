<?php
namespace app\modules\user\assets;

use yii\web\AssetBundle;

class UserAsset extends AssetBundle {
	
	public $sourcePath = '@user';
	public $publishOptions = [
	    'forceCopy' => true,
	];
	
	public $js = [
		'js/script.js'
	];
	
	public $depends = [ 
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset', 
        'yii\bootstrap\BootstrapAsset',
        'app\assets\AppAsset', 
    ];  
	
}

?>