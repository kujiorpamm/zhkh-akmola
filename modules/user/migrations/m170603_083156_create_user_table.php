<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m170603_083156_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'email' => $this->string(150)->notNull(),
            'phone' => $this->string(150),
            'fname' => $this->string(150)->notNull(),
            'lname' => $this->string(150)->notNull(),
            'password' => $this->string(150)->notNull(),
            'authkey' => $this->string(150)->notNull(),
            'is_active' => $this->string(1)->notNull()->defaultValue(0),
        ]);
        
        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
