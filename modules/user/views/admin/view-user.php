<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\ActiveForm;
use app\modules\user\assets\UserAsset;
/* @var $this yii\web\View */
/* @var $model app\modules\user\models\User */

UserAsset::register($this);

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Пользователи'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="workplace">
	
	<div class="heading">
		<h3><?= Html::encode($this->title) ?></h3>
		
	</div>
	
	<div class="application-container">
		<div class="row">
			<div class="col-sm-6">
				<?php $form = ActiveForm::begin(); ?>
				<?= $form->errorSummary($model); ?>
				
				<div class="row">
					<div class="col-sm-6"><label><?=Yii::t('app', 'Статус:')?></label></div> 
					<div class="col-sm-6 clearfix">
						<div class="pull-right">
							<?=$form->field($model, 'role')->dropDownList($roles)->label(false)?>
						</div>
					</div>
				</div>
				<hr/>
				<div class="row">
					<div class="col-sm-6"><label><?=Yii::t('app', 'Пользователь активен:')?></label></div> 
					<div class="col-sm-6 clearfix">
						<div class="pull-right">
							<?=$form->field($model, 'is_active')->dropDownList(['1'=>'Да', '0'=>'Нет'])->label(false)?>
						</div>
					</div>
				</div>
				
				<?=$form->field($model, 'fname')->textInput(['placeholder'=>$model->getAttributeLabel('fname')])?>
				<?=$form->field($model, 'lname')->textInput(['placeholder'=>$model->getAttributeLabel('lname')])?>
				<?=$form->field($model, 'email')->textInput(['placeholder'=>$model->getAttributeLabel('email'), 'disabled'=>true])?>
				<?=$form->field($model, 'phone')-> widget(\yii\widgets\MaskedInput::className(), [
													    'mask' => '+7-999-9999999',
													    'options' => ['class'=>'form-control', 'placeholder'=>$model->getAttributeLabel('phone')]
													])?>
				<?=$form->field($model, 'note')->textArea(['placeholder'=>$model->getAttributeLabel('note')])?>
				<br/>
				<center><input class="btn btn-success" type="submit" value="<?=Yii::t('app', 'Сохранить изменения')?>"/>
				
				<?=Yii::t('app', '<br/><br/>или <a onclick="js:resetPasswordTo(\''.Yii::t('app', 'Вы уверены, что хотите сбросить пароль?').'\', '.$model->id.')" href="#">сбросить пароль</a>')?>
				
				</center>
				
				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
	
	
</div>

