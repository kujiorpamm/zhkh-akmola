<?php
use yii\bootstrap\ActiveForm;
use app\modules\user\assets\UserAsset;
use app\modules\application\controllers\ApplicationController;
use yii\widgets\ListView;

UserAsset::register($this);

?>

<div class="page-index profile">
	<div class="wrap">
		<div class="container">
			<div class="panel">
			<h2><?=Yii::t('app', 'Личный кабинет')?></h2>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<div class="panel">
						<h3><?=Yii::t('app', 'Данные')?></h3>
						
						
						
						<?php $form = ActiveForm::begin(); ?>
						<?= $form->errorSummary($model); ?>
						
						<div class="row">
							<div class="col-sm-6"><label><?=Yii::t('app', 'Статус:')?></label></div> 
							<div class="col-sm-6 clearfix">
								<div class="pull-right">
									<?=$model->roleDescription?>
								</div>
							</div>
						</div>
						<hr/>
						<?=$form->field($model, 'fname')->textInput(['placeholder'=>$model->getAttributeLabel('fname')])?>
						<?=$form->field($model, 'lname')->textInput(['placeholder'=>$model->getAttributeLabel('lname')])?>
						<?=$form->field($model, 'email')->textInput(['placeholder'=>$model->getAttributeLabel('email'), 'disabled'=>true])?>
						<?=$form->field($model, 'phone')-> widget(\yii\widgets\MaskedInput::className(), [
															    'mask' => '+7-999-9999999',
															    'options' => ['class'=>'form-control', 'placeholder'=>$model->getAttributeLabel('phone')]
															])?>
						
						<br/>
						<center><input class="btn btn-success" type="submit" value="<?=Yii::t('app', 'Сохранить изменения')?>"/>
						
						<?=Yii::t('app', '<br/><br/>или <a onclick="js:resetPassword(\''.Yii::t('app', 'Вы уверены, что хотите сбросить пароль?').'\')" href="#">сбросить пароль</a>')?>
						
						
						
						</center>
						
						<?php ActiveForm::end(); ?>
					</div>
				</div>
				<div class="col-sm-8">
					<div class="panel">
					<h3>
						
						<div class="clearfix">
							<div class="pull-left"><?=Yii::t('app', 'Мои заявки')?></div>
							<div class="pull-right">
								<a class="red fix1" href="/report"><?=Yii::t('app', 'Сообщить о проблеме')?></a>
							</div>
						</div>	
					</h3>
					
					<table class="table">
					
					<?php
						
						echo ListView::widget([
						    'dataProvider' => $dp,
						    'itemView' => '@app/modules/application/views/application/_item_application_profile',
						]);
						
						
					?>
					
					</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>