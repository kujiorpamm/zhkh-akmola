<?php

namespace app\modules\user;

/**
 * user module definition class
 * Настроить web.php
 * Настроить console.php
 * > yii migrate/up --migrationPath=@app/modules/user/migrations
 * > yii migrate --migrationPath=@yii/rbac/migrations/
 * > yii user/command
 */
class Module extends \yii\base\Module implements \yii\base\BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\user\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
		$this->setAliases([ 
            '@user' => __DIR__ . '/assets' 
        ]);
    }
    
    public function bootstrap($app)
    {
        
        if ($app instanceof \yii\console\Application) {
		    $this->controllerNamespace = 'app\modules\user\commands';
		}
        
    }
    
}
