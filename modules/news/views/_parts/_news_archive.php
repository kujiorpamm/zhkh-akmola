<div class="row news-archive">
	<div class="col-sm-10 col-xs-12">
		
		<div class="title">
			<span class="moment-date-full visible-xs"><?=$model->date?></span> <a href="/news/<?=$model->id?>"><?=$model->title?></a>
		</div>
	</div>
	<div class="col-sm-2 hidden-xs">
		<span class="moment-date-full"><?=$model->date?></span>
	</div>
</div>