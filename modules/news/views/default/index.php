<?php
	use Yii;
?>

<div class="default-page news-index-page">

	<div class="wrap">
		<div class="container">
			<h2><?=Yii::t('app', 'Архив новостей')?></h2>
			
			<?php
				$last_month = "";
				Yii::$app->formatter->locale = 'ru_RU.UTF-8';
				//echo $last_year; 
				foreach($models as $md) {
					
					if($last_month !== $md->date_month_year) {
						$last_month = $md->date_month_year;
						echo "<h3 class='moment-year-month'>".$md->date."</h3>";
					}
					
					echo $this->render("@app/modules/news/views/_parts/_news_archive", ['model'=>$md]);
					
				}
			?>
			
		</div>
	</div>
	
</div>