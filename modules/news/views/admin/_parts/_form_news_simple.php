<?php
	use yii\bootstrap\ActiveForm;
?>

<?php $form = ActiveForm::begin(['action'=>$action]); ?>
<?= $form->errorSummary($model); ?>
	
	<?=$form->field($model, 'category_id')->dropDownList(['1'=>'Новость', '2'=>'Отчет'])->label(false)?>
	<?=$form->field($model, 'title_ru')->textInput(['placeholder'=>$model->getAttributeLabel('title_ru')])->label(false)?>
	<?=$form->field($model, 'title_kz')->textInput(['placeholder'=>$model->getAttributeLabel('title_kz')])->label(false)?>
	
	<?= $form->field($model, 'text_ru')->widget(\yii\redactor\widgets\Redactor::className(), [
			'clientOptions' => [
				'plugins' => ['fontcolor','imagemanager','filemanager', 'video'],
				'minHeight' => 500,
				'lang' => 'ru',
				'minHeight' => '300px',
				'placeholder' => $model->getAttributeLabel('text_ru')
			]
		])->label(false);?>
	<?= $form->field($model, 'text_kz')->widget(\yii\redactor\widgets\Redactor::className(), [
			'clientOptions' => [
				'plugins' => ['fontcolor','imagemanager','filemanager', 'video'],
				'minHeight' => 500,
				'lang' => 'ru',
				'minHeight' => '300px',
				'placeholder' => $model->getAttributeLabel('text_kz')
			]
		])->label(false);?>
	
	<button type="submit" class="btn btn-success btn-block"> <?=Yii::t('app', 'Добавить')?> </button>
	
<?php ActiveForm::end(); ?>