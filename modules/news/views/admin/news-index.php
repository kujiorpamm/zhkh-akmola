<?php
	use yii\helpers\Html;
	use kartik\grid\GridView;
	
	$this->title = Yii::t('app', 'Новости сайта');
	$this->params['breadcrumbs'][] = ['label' => 'Администрирование', 'url' => ['/new']];
	$this->params['breadcrumbs'][] = $this->title;
?>
<?=$this->render('_parts/_add_news_modal', ['model'=>$model])?>

<div class="container">
	

<div class="archive-view">
	
	
	<h2 class="border"><?=Yii::t('app', 'Новости')?></h2>
		
		<?=GridView::widget([
		    'dataProvider'=> $dp,
		    'columns' => [
		    		'id',
		    		'date_human',
		    		'title_ru',
		    		['class'=>'\kartik\grid\DataColumn', 'attribute'=>'link_view', 'format'=>'html', 'hAlign'=>'center'],
		    		['class'=>'\kartik\grid\DataColumn', 'attribute'=>'link_edit', 'format'=>'html', 'hAlign'=>'center'],
		    		['class'=>'\kartik\grid\DataColumn', 'attribute'=>'link_delete', 'format'=>'html', 'hAlign'=>'center'],
		    	],
		    'responsive'=>true,
		    'hover'=>true,
		    'striped'=>false,
		    'pjax'=>false,
		    'toolbar'=> false,
		    'export'=>[
		        'fontAwesome'=>false,
		        'options' => [
		        	'class' => 'btn-export',
		        ]
		    ],
		    'exportConfig'=> [
				GridView::EXCEL => true
			],
		    'panel'=>[
	        	'type'=>GridView::TYPE_DEFAULT,
	        	'footer' => false
		    ],
		    'panelHeadingTemplate' => $this->render('_parts/_form_search', ['dp'=>$dp, 'search'=>$search]),
		]);?>
	</div>
</div>