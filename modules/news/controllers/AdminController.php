<?php

namespace app\modules\news\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;


use app\modules\news\models\News;

/**
 * Default controller for the `news` module
 */
class AdminController extends Controller
{
    public $layout = "@app/modules/workplace/views/layouts/main";
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin', 'moderator'],
                    ]
                    
                ],
            ]
        ];
    }
    
    public function actionIndex() {
		
		$model = new News;
		
		if($_POST['search-word']) $search = $_POST['search-word'];
		
		if($search) {
			$query = News::find()->where(['like', 'title_ru', "{$search}"])
				->orWhere(['like', 'title_kz', "{$search}"]);
		} else {
			$query = News::find();
		}
		
		$dp = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
				'pageSize' => 20,
			],
			'sort' => [
				'defaultOrder' => [
					'date_human' => SORT_DESC
				],
				'attributes' => [
					'date_human' => [
						'desc' => ['date'=>SORT_DESC],
						'asc' => ['date'=>SORT_ASC],
					]
				]
			]
		]);
		
		return $this->render('news-index', ['model'=>$model, 'dp'=>$dp, 'search'=>$search]);
	}
	
	public function actionEditNews($id) {
		$model = News::findOne(['id'=>$id]);
		
		if($model->load(Yii::$app->request->post())) {
			if($model->save()) {
				return $this->redirect("/news/{$model->id}");
			} else {
				print_r($model->getErrors()); exit;
				//throw new e
			}			
		}
		
		return $this->render('news-edit', ['model'=>$model]);
	}
	
	public function actionAddNews() {
		$model = new News;
		
		$model->date = date('Y-m-d H:i:s');
		
		if($model->load(Yii::$app->request->post())) {
			if($model->save()) {
				return $this->redirect("/news/{$model->id}");
			} else {
				print_r($model->getErrors()); exit;
				//throw new e
			}
			
		}		
	}
	
	public function actionDeleteNews($id) {
		News::findOne(['id'=>$id])->delete();
		return $this->redirect("/news/admin");
	}
    
}
