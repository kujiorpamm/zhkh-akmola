<?php

namespace app\modules\news\controllers;

use yii\web\Controller;
use app\modules\news\models\News;

/**
 * Default controller for the `news` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $models = News::find()->where(['=', 'category_id', 1])
        	->select(['date', 'title_ru', 'title_kz', 'id'])->indexBy('date')->orderBy(['date'=> SORT_DESC])->all();
        //print_r($models); exit;
        return $this->render('index', ['models'=>$models]);
    }
    
    public function actionView($id) {
		$model = News::findOne(['id'=>$id]);
		return $this->render('view', ['model'=>$model]);
	}
}
