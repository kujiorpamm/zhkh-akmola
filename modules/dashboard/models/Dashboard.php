<?php

namespace app\modules\dashboard\models;

use Yii;

/**
 * This is the model class for table "dashboard".
 *
 * @property integer $id
 * @property string $date
 * @property double $v1
 * @property double $v2
 * @property double $v3
 * @property double $v4
 * @property double $v5
 * @property double $v6
 * @property double $v7
 * @property double $v8
 * @property double $v9
 * @property double $v10
 * @property double $v11
 * @property double $v12
 * @property double $v13
 * @property double $v14
 * @property double $v15
 * @property double $v16
 * @property double $v17
 * @property double $v18
 * @property double $v19
 * @property double $v20
 * @property double $v21
 * @property double $v22
 * @property double $v23
 * @property double $v24
 * @property double $v25
 * @property double $v26
 * @property double $v27
 * @property double $v28
 * @property double $v29
 * @property double $v30
 * @property double $v31
 * @property double $v32
 * @property double $v33
 * @property double $v34
 * @property double $v35
 * @property double $v36
 * @property double $v37
 * @property double $v38
 * @property double $v39
 * @property double $v40
 * @property double $v41
 * @property double $v42
 * @property double $v43
 * @property double $v44
 * @property double $v45
 * @property double $v46
 * @property double $v47
 * @property double $v48
 * @property double $v49
 * @property double $v50
 * @property double $v51
 * @property double $v52
 * @property double $v53
 * @property double $v54
 * @property double $v55
 * @property double $v56
 * @property double $v57
 * @property double $v58
 * @property double $v59
 * @property double $v60
 * @property double $v61
 * @property double $v62
 * @property double $v63
 */
class Dashboard extends \yii\db\ActiveRecord
{
    public $date_human;
    public $agree;
    
    public static function tableName()
    {
        return 'dashboard';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'author_id', 'v1', 'v2', 'v3', 'v4', 'v5', 'v6', 'v7', 'v8', 'v9', 'v10', 'v11', 'v12', 'v13', 'v14', 'v15', 'v16', 'v17', 'v18', 'v19', 'v20', 'v21', 'v22', 'v23', 'v24', 'v25', 'v26', 'v27', 'v28', 'v29', 'v30', 'v31', 'v32', 'v33', 'v34', 'v35', 'v36', 'v37', 'v38', 'v39', 'v40', 'v41', 'v42', 'v43', 'v44', 'v45', 'v46', 'v47', 'v48', 'v49', 'v50', 'v51', 'v52', 'v53', 'v54', 'v55', 'v56', 'v57', 'v58', 'v59', 'v60', 'v61', 'v62', 'v63'], 'required'],
            //[['date'], 'safe'],
            [['v1', 'v2', 'v3', 'v4', 'v5', 'v6', 'v7', 'v8', 'v9', 'v10', 'v11', 'v12', 'v13', 'v14', 'v15', 'v16', 'v17', 'v18', 'v19', 'v20', 'v21', 'v22', 'v23', 'v24', 'v25', 'v26', 'v27', 'v28', 'v29', 'v30', 'v31', 'v32', 'v33', 'v34', 'v35', 'v36', 'v37', 'v38', 'v39', 'v40', 'v41', 'v42', 'v43', 'v44', 'v45', 'v46', 'v47', 'v48', 'v49', 'v50', 'v51', 'v52', 'v53', 'v54', 'v55', 'v56', 'v57', 'v58', 'v59', 'v60', 'v61', 'v62', 'v63'], 'number'],
            [['date'], 'validateDate'],
            [['agree'], 'required', 'requiredValue'=>1, 'message'=>Yii::t('app', 'Подтвердите верность введенных значений')]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'date' => Yii::t('app', 'Дата внесения показаний приборов'),
            'date_human' => Yii::t('app', 'Дата внесения показаний приборов'),
            'author_id' => Yii::t('app', 'Автор'),
            'author' => Yii::t('app', 'Автор'),
            'v1' => Yii::t('app', 'Вещества, присутствующие в воде в результате хлорирования->Хлор остаточный'),
            'v2' => Yii::t('app', 'Вещества, присутствующие в воде в результате хлорирования->Хлороформ'),
            'v3' => Yii::t('app', 'Органолептические показатели->Цветность'),
            'v4' => Yii::t('app', 'Органолептические показатели->Мутность'),
            'v5' => Yii::t('app', 'Органолептические показатели->Запах'),
            'v6' => Yii::t('app', 'Органолептические показатели->Привкус'),
            'v7' => Yii::t('app', 'Неорганические вещества->Железо'),
            'v8' => Yii::t('app', 'Неорганические вещества->Медь'),
            'v9' => Yii::t('app', 'Неорганические вещества->Фториды'),
            'v10' => Yii::t('app', 'Неорганические вещества->Нитрит-ион'),
            'v11' => Yii::t('app', 'Неорганические вещества->Аммиак'),
            'v12' => Yii::t('app', 'Неорганические вещества->Нитраты'),
            'v13' => Yii::t('app', 'Неорганические вещества->Хлориды'),
            'v14' => Yii::t('app', 'Неорганические вещества->Натрий'),
            'v15' => Yii::t('app', 'Неорганические вещества->Сульфаты'),
            'v16' => Yii::t('app', 'Неорганические вещества->Кальций'),
            'v17' => Yii::t('app', 'Обобщенные показатели->Водородный показатель'),
            'v18' => Yii::t('app', 'Обобщенные показатели->Общая минерализация'),
            'v19' => Yii::t('app', 'Обобщенные показатели->Жесткость общая'),
            'v20' => Yii::t('app', 'Обобщенные показатели->Окисляемость перманганатная'),
            'v21' => Yii::t('app', 'Обобщенные показатели->Нефтепродукты'),
            'v22' => Yii::t('app', 'Микробиологические показатели->Общие колиформные бактерии'),
            'v23' => Yii::t('app', 'Микробиологические показатели->Термотолерантные колиформные бактерии'),
            'v24' => Yii::t('app', 'Микробиологические показатели->Общее микробное число'),
            'v25' => Yii::t('app', 'Выпор дисцилята'),
            'v26' => Yii::t('app', 'Выработка минеральной воды'),
            'v27' => Yii::t('app', 'Техническая вода - 1'),
            'v28' => Yii::t('app', 'Техническая вода - 2'),
            'v29' => Yii::t('app', 'Техническая вода - 3'),
            'v30' => Yii::t('app', 'Горячая вода - 1'),
            'v31' => Yii::t('app', 'Горячая вода - 2'),
            'v32' => Yii::t('app', 'Горячая вода - 3 '),
            'v33' => Yii::t('app', 'Питьевая вода - 1'),
            'v34' => Yii::t('app', 'Питьевая вода - 2'),
            'v35' => Yii::t('app', 'Питьевая вода - 3'),
            'v36' => Yii::t('app', 'Верхняя зона города->Расход горячей воды -1'),
            'v37' => Yii::t('app', 'Верхняя зона города->Расход горячей воды -2'),
            'v38' => Yii::t('app', 'Верхняя зона города->Подпитка'),
            'v39' => Yii::t('app', 'Нижняя зона города->Расход горячей воды -1'),
            'v40' => Yii::t('app', 'Нижняя зона города->Расход горячей воды -2'),
            'v41' => Yii::t('app', 'Нижняя зона города->Подпитка'),
            'v42' => Yii::t('app', 'От ТЭЦ1 до новых микрорайонов->Расход горячей воды -1'),
            'v43' => Yii::t('app', 'От ТЭЦ1 до новых микрорайонов->Расход горячей воды -2'),
            'v44' => Yii::t('app', 'От ТЭЦ1 до новых микрорайонов->Подпитка'),
            'v45' => Yii::t('app', '22 микрорайон->Расход горячей воды -1'),
            'v46' => Yii::t('app', '22 микрорайон->Расход горячей воды -2'),
            'v47' => Yii::t('app', '22 микрорайон->Подпитка'),
            'v48' => Yii::t('app', 'Верхняя зона города->Давление-1'),
            'v49' => Yii::t('app', 'Верхняя зона города->Давление-2'),
            'v50' => Yii::t('app', 'Нижняя зона города->Давление-1'),
            'v51' => Yii::t('app', 'Нижняя зона города->Давление-2'),
            'v52' => Yii::t('app', 'От ТЭЦ1 до новых микрорайонов->Давление-1'),
            'v53' => Yii::t('app', 'От ТЭЦ1 до новых микрорайонов->Давление-2'),
            'v54' => Yii::t('app', '22 микрорайон->Давление-1'),
            'v55' => Yii::t('app', '22 микрорайон->Давление-2'),
            'v56' => Yii::t('app', 'Верхняя зона города->Температура-1'),
            'v57' => Yii::t('app', 'Верхняя зона города->Температура-2'),
            'v58' => Yii::t('app', 'Нижняя зона города->Температура-1'),
            'v59' => Yii::t('app', 'Нижняя зона города->Температура-2'),
            'v60' => Yii::t('app', 'От ТЭЦ1 до новых микрорайонов->Температура-1'),
            'v61' => Yii::t('app', 'От ТЭЦ1 до новых микрорайонов->Температура-2'),
            'v62' => Yii::t('app', '22 микрорайон->Температура-1'),
            'v63' => Yii::t('app', '22 микрорайон->Температура-2'),
        ];
    }
    
    public function validateDate($attribute, $params, $validator) {
		if(self::find()->where(['date'=>$this->date])->count() > 0) {
			$date = Yii::$app->formatter->asDate($this->date, 'php:d.m.Y');
			$this->addError($attribute, Yii::t('app', 'Данные на указанное число: {0} , уже присутствуют в базе', [$date]));
		}
	}
    
    public function getAuthor() {
		return \app\modules\user\models\User::findOne(['id'=>$this->author_id]);
	}
    
    public function getRemoveLink() {
		return \yii\helpers\Html::a("<i class='glyphicon glyphicon-remove'/>", \yii\helpers\Url::to('/dashboard/admin/delete?id='.$this->id, true), ['class'=>'confirm-link'] );
	}
    
    public function afterFind() {
    	if($this->date == date('Y-m-d')) {
			$this->date_human = Yii::t('app', 'Сегодня');
		} else {
			$this->date_human =  \Yii::$app->formatter->asDate($this->date, 'php:d.m.Y');
		}
			
	}
    
}
