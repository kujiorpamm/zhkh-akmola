<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\map\models\MapTypes */

$this->title = 'Редактирование: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Map Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="workplace">

    <div class="heading">
        <h3><?= Html::encode($this->title) ?></h3>
	</div>

	<div class="application-container">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
	</div>

</div>
