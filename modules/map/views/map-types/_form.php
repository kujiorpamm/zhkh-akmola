<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\map\models\MapTypes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="map-types-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div id="cropit_icon">
        <?= $form->field($model, 'icon')->widget(\kujiorpamm\cropit\widgets\CropitWidget::className(), [
            'pluginOptions' => [
                'width' => 100,
                'height' => 100,
                'smallImage' => 'allow'
        ]])->label('Иконка 100x100 px');?>
    </div>

    <?= $form->field($model, 'sort')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
