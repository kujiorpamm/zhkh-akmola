<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\map\assets\MapAsset;
MapAsset::register($this);

$this->registerJsFile('/js/map/add-object.js');

/* @var $this yii\web\View */
/* @var $model app\modules\map\models\MapObjects */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="map-objects-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-sm-5 form-data">
            <?= $form->field($model, 'coords')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'type_id')->dropDownList($types) ?>

            <?= $form->field($model, 'data')->widget(\yii\redactor\widgets\Redactor::className(), [
                'clientOptions' => [
                    'plugins' => ['fontcolor','imagemanager','filemanager', 'video'],
                    'minHeight' => 500,
                    'lang' => 'ru',
                    'minHeight' => '300px'
                ]
            ])->label(false);?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
        <div class="col-sm-7">
            <div id="map" style="height: 515px;"></div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
