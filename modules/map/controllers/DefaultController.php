<?php

namespace app\modules\map\controllers;

use Yii;
use yii\web\Controller;
use app\modules\map\models\MapTypes;
use app\modules\map\models\MapObjects;

/**
 * Default controller for the `map` module
 */
class DefaultController extends Controller
{

    public $layout = "@app/modules/workplace/views/layouts/main";

    public function actionIndex()
    {
        $layers = MapTypes::find()->all();
        $geojsons = [];

        foreach ($layers as $_layer) {
            $objects = MapObjects::find()->where(['type_id'=>$_layer->id])->all();
            $geojson = [
                'type' => 'FeatureCollection',
                'features' => [],
                'name' => 'layer-' . $_layer->id,
                'icon' => $_layer->icon
            ];
            foreach ($objects as $obj) {
                $coords = explode(',', $obj->coords);
                $geojson['features'][] = [
                    'type' => 'Feature',
                    'geometry' => [
                        'type' => 'Point',
                        'coordinates' => [
                            (float) $coords[0],
                            (float) $coords[1],
                        ]
                    ],
                    'properties' => [
                        'title' => $_layer->name,
                        'description' => $obj->data
                    ]
                ];
            }
            $geojsons[] = $geojson;
        }


        return $this->render('index', [
            'layers' => $layers,
            'geojsons' => $geojsons
        ]);
    }

    public function actionGetLayer($id) {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $layer = MapTypes::find()->where(['id'=>$id])->one();
        $objects = MapObjects::find()->where(['type_id'=>$layer->id])->all();
        $geojson = [
            'type' => 'FeatureCollection',
            'features' => []
        ];
        foreach ($objects as $obj) {
            $coords = explode(',', $obj->coords);
            $geojson['features'][] = [
                'type' => 'Feature',
                'geometry' => [
                    'type' => 'Point',
                    'coordinates' => [
                        (float) $coords[0],
                        (float) $coords[1],
                    ]
                ],
                'properties' => [
                    'title' => $layer->name,
                    'description' => $obj->data
                ]
            ];
        }
        return $geojson;
    }
}
