<?php

namespace app\modules\map\models;

use Yii;

/**
 * This is the model class for table "map_objects".
 *
 * @property integer $id
 * @property string $coords
 * @property integer $type_id
 * @property string $data
 *
 * @property MapTypes $type
 */
class MapObjects extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'map_objects';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['coords', 'type_id', 'data'], 'required'],
            [['type_id'], 'integer'],
            [['data'], 'string'],
            [['coords'], 'string', 'max' => 128],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => MapTypes::className(), 'targetAttribute' => ['type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'coords' => Yii::t('app', 'Координаты'),
            'type_id' => Yii::t('app', 'Тип'),
            'data' => Yii::t('app', 'Информация'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(MapTypes::className(), ['id' => 'type_id']);
    }
}
