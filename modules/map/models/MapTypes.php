<?php

namespace app\modules\map\models;

use Yii;

/**
 * This is the model class for table "map_types".
 *
 * @property integer $id
 * @property string $name
 * @property string $icon
 * @property integer $sort
 *
 * @property MapObjects[] $mapObjects
 */
class MapTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'map_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'icon'], 'required'],
            [['sort'], 'integer'],
            [['name'], 'string', 'max' => 256],
            [['icon'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Наименование'),
            'icon' => Yii::t('app', 'Иконка для отображения'),
            'sort' => Yii::t('app', 'Сортировка'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMapObjects()
    {
        return $this->hasMany(MapObjects::className(), ['type_id' => 'id']);
    }
}
