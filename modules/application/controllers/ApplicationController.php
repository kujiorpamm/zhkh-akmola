<?php

namespace app\modules\application\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use kujiorpamm\cropit\widgets\CropitWidget;

use app\modules\application\models\Application;
use app\modules\application\models\ApplicationTypes;
use app\modules\application\models\ApplicationAssignment;
use app\modules\application\models\ApplicationCommends;
use app\modules\organization\models\Organization;


class ApplicationController extends Controller
{	 
    
    public $path = '@app/modules/application/views/';
    
    public static function render_commend($model) {
		return Yii::$app->controller->renderPartial('@app/modules/application/views/application/_item_commend', ['model'=>$model]);
	}
	
	public static function render_application_adm($model) {
		return Yii::$app->controller->renderPartial('@app/modules/application/views/application/_item_app_adm', ['model'=>$model]);
	}
    
}
