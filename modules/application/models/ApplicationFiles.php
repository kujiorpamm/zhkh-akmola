<?php

namespace app\modules\application\models;

use Yii;

/**
 * This is the model class for table "application_files".
 *
 * @property integer $id
 * @property string $ext
 * @property string $src
 * @property integer $type
 */
class ApplicationFiles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'application_files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ext', 'src', 'type', 'application_id'], 'required'],
            [['type', 'application_id'], 'integer'],
            [['ext'], 'string', 'max' => 30],
            [['src'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ext' => Yii::t('app', 'Ext'),
            'src' => Yii::t('app', 'Src'),
            'type' => Yii::t('app', 'Type'),
        ];
    }
	
	public function afterSave($insert, $changedAttributes){
	    parent::afterSave($insert, $changedAttributes);
	    
	    if($this->type == 2) {
			$model = ApplicationFiles::find()->where(['=', 'application_id', $this->application_id])->andWhere(['=', 'type', '2'])->andWhere(['!=', 'id', $this->id])->one();
	    	if($model) $model->delete();
		} else {
			$model = ApplicationFiles::find()->where(['=', 'application_id', $this->application_id])->andWhere(['=', 'type', '1'])->andWhere(['!=', 'id', $this->id])->one();
	    	if($model) $model->delete();
		}
	    
	    
	}
    
}
