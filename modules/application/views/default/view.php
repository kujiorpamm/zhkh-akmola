<?php
	use yii\bootstrap\ActiveForm;
	use yii\bootstrap\Html;
	use kartik\date\DatePicker;

	use kujiorpamm\cropit\widgets\CropitWidget;
	use app\modules\application\assets\ApplicationCreateAsset;
	use app\modules\application\controllers\ApplicationController;

	ApplicationCreateAsset::register($this);

	$this->title = $model->id;
	$this->params['breadcrumbs'][] = Yii::t('app', 'Заявка №{0}', [$model->id]);

	if(Yii::$app->user->isGuest) {
		$canAdmin = false;
	} else {
		$canAdmin = Yii::$app->user->identity->canAdmin();
	}



?>

<script>
	var is_view = true;
	var app_coords = [<?=$model->coords?>];
</script>

<div class="page-index">
	<div class="wrap">
		<div class="container">

			<div class="application-view">
				<div class="view-block-container application-info white">
					<div class="heading">
						<div class="clearfix">
							<div class="pull-left"> <?=Yii::t('app', 'Заявка №{0} от {1}, планируемое завершение: {2}', [$model->id, substr($model->date_created, 0, 10), substr($model->date_plane_end, 0, 10)])?></div>
							<div class="pull-right status">
								<?=Yii::t('app', 'Статус:')?> <?=$model->statusName?>
								<?php if($model->status == 3) echo " {$model->date_end}"?>
							</div>
						</div>
					</div>
					<div class="content">
						<div class="row">
							<div class="col-sm-8">
								<div class="type"><?=$model->fulltype?></div>
								<div class="text"><?=$model->description?></div>
								<div class="orgs">
									<!--НАЧАЛО НАЗНАЧЕННЫЕ ИСПОЛНИТЕЛИ-->
									<?=Yii::t('app', 'Назначенные исполнители: ')?>
									<?php
										if($organizations = $model->organizations) {
											foreach($organizations as $org) {
												echo "<a href='/organization/default/view?id={$org->id}'>$org->name</a> ";
											}
										} else {
											echo Yii::t('app', 'Пока нет...');
										}

									?>
									<!--КОНЕЦ НАЗНАЧЕННЫЕ ИСПОЛНИТЕЛИ-->
								</div>
								<div class="author">
									<?=Yii::t('app', 'Автор обращения: ') . "<span>" . $model->authorName?> </span> <br/>
								</div>


							</div>
							<div class="col-sm-4">
								<div class="image">
									<img class="img img-responsive img-popup" src="<?=$model->imageBefore?>"/>
								</div>
							</div>
						</div>
					</div>



				</div>

				<div class="row">
					<div class="col-sm-8">

						<!--НАЧАЛО КОММЕНТАРИИ-->
						<div class="commends-block">
							<h3><?=Yii::t('app', 'Комментарии')?></h3>
							<?php

								if($commends = $model->commends) {
									foreach($model->commends as $cmd) {
										echo ApplicationController::render_commend($cmd);
									}
								} else {
									echo "<div class='empty'>".Yii::t('app', 'Пока нет...')."</div>";
								}


							?>
						</div>

						<?php if(Yii::$app->user->identity && Yii::$app->user->identity->canCommendApplication($model->id)):?>
							<div class="note"><?=Yii::t('app', 'Чтобы оставить комментарий, откройте заявку через <a class="red" href="/workplace/default/view?id={0}">рабочий стол</a>', [$model->id])?></div>
						<?php endif; ?>
						<!--КОНЕЦ КОММЕНТАРИИ-->

					</div>
					<div class="col-sm-4">

						<!--НАЧАЛО КАРТА-->
						<div id="ymap_view" class="view-block-container view-block-container application-view-map map"></div>
						<!--КОНЕЦ КАРТА-->

						<!--НАЧАЛО ОРГАНИЗАЦИИ-->
						<div class="view-block-container white executors">
							<div class="heading"><?=Yii::t('app', 'Исполнители')?></div>
							<table class="table no-borders">
								<?php
									if($organizations = $model->assignments) {
										foreach($organizations as $org) {

											if($canAdmin) {
												echo "<tr>
														<td>{$org->organization->name} <span>{$org->contract->name}</span></td>
														<td><a class='confirm-link' href='/workplace/moderator/unasign-organization?id={$model->id}&org={$org->organization->id}'><span class='glyphicon glyphicon-remove'></span></a></td>
													</tr>";
											} else {
												echo "<tr>
														<td>{$org->organization->name} <span>{$org->contract->name}</span></td>
														<td></td>
													</tr>";
											}


										}
									} else {
										echo Yii::t('app', 'Пока нет...');
									}

								?>
							</table>
						</div>
						<!--КОНЕЦ ОРГАНИЗАЦИИ-->


					</div>
				</div>

			</div>

		</div>
	</div>
</div>
