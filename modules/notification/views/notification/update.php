<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Notifications */

$this->title = 'Update Notifications: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Notifications', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="workplace">

	<div class="heading">
		<h3><?=Yii::t('app', 'Добавить новое уведомление')?></h3>
	</div>

	<div class="application-container dashboard">
		<div class="panel">

            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>

		</div>
	</div>

</div>
