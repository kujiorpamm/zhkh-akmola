<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Notifications */

$this->title = 'Create Notifications';
$this->params['breadcrumbs'][] = ['label' => 'Notifications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="workplace">

	<div class="heading">
		<h3><?=Yii::t('app', 'Добавить новое уведомление')?></h3>
	</div>

	<div class="application-container dashboard">
		<div class="panel">

            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>

		</div>
	</div>

</div>
