<div class="workplace">
	
	<div class="heading">
		<h3><?=Yii::t('app', 'Уведомления')?></h3>		
	</div>
	
	<div class="application-container dashboard">
		Раздел находится в разработке.
	</div>
	
	<div class="application-container dashboard">
		<div class="panel">
			
			<table class="table">
			
			<?php foreach($notifications['uncompleted_need_reason']  as $uncompleted) : ?>
				
				<tr>
					<td><?=Yii::t('app', '- Необходимо указать причину для заявки <b>№{0}</b> от {1}', [$uncompleted->id, $uncompleted->date_created])?> <br /></td>
					<td><?=Yii::t('app', '<a href="/workplace/default/view?id={0}"> Заявка №{0}</a>', [$uncompleted->id])?></td>
				</tr>
				
				
				
			<? endforeach ?>
			
			</table>
			
		</div>
	</div>
	
</div>