<?php

namespace app\modules\notification\widgets;

use yii\base\Widget;
use app\modules\notification\assets\NotifyAsset;
use app\modules\application\models\Application;

class AdminLinkWidget extends Widget
{

	public function init()
    {
        parent::init();
    }
    
    public function run()
    {
        // Незавершенные заявки более месяца
        $month_ago_date = date('Y-m-d', strtotime("-30 days"));
        $uncompleted_applications_count = Application::find()
        		->where(['IN', 'status', [0, 1, 2]])
        		->andWhere(['<=', 'DATE(date_created)', $month_ago_date])
        		->andWhere(['IS', 'uncomplete_reason', NULL])
        		->count();
        
        $view = $this->getView();
        NotifyAsset::register($view);
        return \Yii::t('app', "<a class='notify-link-bar' href='/notification'> Уведомления <span>{0}</span></a>", [$uncompleted_applications_count]);
    }
	
}
