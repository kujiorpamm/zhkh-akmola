<?php
	use app\modules\statistics\assets\ChartAsset_main;
	use app\modules\workplace\assets\WorkplaceAsset;
	use yii\helpers\Html;
	use yii\bootstrap\ActiveForm;
	use kartik\date\DatePicker;
	use kartik\select2\Select2;

	ChartAsset_main::register($this);
    \widgets\mapbox\MapboxWidget::widget(['page' => 'statistics-map']);
	WorkplaceAsset::register($this);
	//$this->registerCssFile('@app/modules/workplace/assets/css/base-workplace.less');

?>

<div class="workplace default">

    <div class="heading">
        <h3><?=Yii::t('app', 'Статистика')?></h3>

    </div>

    <div class="application-container dashboard">

        <div class="panel search">

            <div class="content">
                <?php $form = ActiveForm::begin(['id'=>'form-print', 'action'=>'/statistics/default/print']); ?>

                <input type="hidden" name="img1"/>
                <input type="hidden" name="img2"/>
                <input type="hidden" name="img3"/>
                <input type="hidden" name="img4"/>
                <input type="hidden" name="img5"/>
                <input type="hidden" name="val1"/>
                <input type="hidden" name="val2"/>
                <input type="hidden" name="val3"/>
                <input type="hidden" name="val4"/>
                <input type="hidden" name="val5"/>
                <input type="hidden" name="val6"/>
                <input type="hidden" name="val7"/>
                <input type="hidden" name="val8"/>

                <div class="row">
                    <div class="col-sm-3">
                        <!--Дата от-->
                        <div class="form-group">

                            <?=DatePicker::widget([
                                'model' => $model,
                                'attribute' => 'date1',
                                'attribute2' => 'date2',
                                'options' => ['placeholder' => Yii::t('app', 'От даты')],
                                'options2' => ['placeholder' => Yii::t('app', 'До даты')],
                                'type' => DatePicker::TYPE_RANGE,
                                'form' => $form,
                                'separator' => Yii::t('app', 'до'),
                                'pluginOptions' => [
                                    'todayHighlight' => true,
                                    'todayBtn' => true,
                                    'format' => 'dd.mm.yyyy',
                                    'autoclose' => true,
                                ]
                            ]);?>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <?=$form->field($model, 'application_type')->widget(Select2::classname(), [
                                'data' => $model->types,
                                'options' => ['placeholder' => Yii::t('app', 'Выберите типы'), 'multiple' => true],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ])->label(false);?>
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <button id="filter" type="button" class="btn btn-success btn-block"><?=Yii::t('app', 'Фильтр')?></button>
                    </div>

                    <div class="col-sm-2">
<!--                        <button id="pdf" type="button" class="btn btn-success">PDF</button>-->
                    </div>
                </div>




                <?php ActiveForm::end(); ?>
            </div>

        </div>

        <div class="panel">
            <div class="row">
                <div class="col-md-8">


                    <div class="chart">
                        <canvas class="canvas" id="chart1"></canvas>
                    </div>


                </div>
                <div class="col-md-4">
                    <div class="board">

                        <div class="row">
                            <div class="col-md-6 col-sm-3">
                                <div class="stats center">
                                    <div class="item">
                                        <div class="value" id="val-1">99</div>
                                        <div class="text"><?=Yii::t('app', 'Всего<br/>заявок')?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-3">
                                <div class="stats center">
                                    <div class="item">
                                        <div class="value" id="val-2">99</div>
                                        <div class="text"><?=Yii::t('app', 'Решено<br/>заявок')?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-3">
                                <div class="stats center">
                                    <div class="item">
                                        <div class="value" id="val-3">~99</div>
                                        <div class="text"><?=Yii::t('app', 'В день<br/>заявок')?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-3">
                                <div class="stats center">
                                    <div class="item">
                                        <div class="value" id="val-4">~99</div>
                                        <div class="text"><?=Yii::t('app', 'В месяц<br/>заявок')?></div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="stats-row">
                            <div class="item">
                                <div class="name"><?=Yii::t('app', 'Чаще всего типы:')?></div>
                                <div class="value" id="val-5"> Прорыв трубы (12%), Мусор (11%) </div>
                            </div>
                            <div class="item">
                                <div class="name"><?=Yii::t('app', 'Чаще всего районы:')?></div>
                                <div class="value" id="val-6"> 12 мкр (12%), 21 мкр (11%) </div>
                            </div>
                            <div class="item">
                                <div class="name"><?=Yii::t('app', 'Пик обращений:')?></div>
                                <div class="value" id="val-7"> Август 2017 </div>
                            </div>
                            <div class="item">
                                <div class="name"><?=Yii::t('app', 'Больше всех учавствовал:')?></div>
                                <div class="value" id="val-8"> МАЭК </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>


        <div class="row map-container">
            <div class="col-sm-12">
                <div class="panel">
                    <div id="map" class="map" style="height: 400px;"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8">
                <div class="panel">
                    <div class="chart">
                        <canvas class="canvas" id="chart2"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="panel">
                    <div class="chart">
                        <canvas class="canvas" id="chart3"></canvas>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="panel">
                    <div class="chart">
                        <canvas class="canvas high" id="chart4"></canvas>
                    </div>
                </div>
            </div>
        </div>

    </div>




</div>
