<?php
	use yii\helpers\Html;
	use yii\bootstrap\ActiveForm;
	use kartik\date\DatePicker;	
?>

<div class="workplace">
	<div class="heading">
		<h3><?=Yii::t('app', 'Отчетная статистика')?></h3>		
	</div>
	
	<div class="application-container dashboard">
		<div class="panel search">
			
			<div class="content">
				<?php $form = ActiveForm::begin(['id'=>'form-print']); ?>
					
					
							
					<div class="row">
						<div class="col-sm-3">
							<!--Дата от-->
							<div class="form-group">
								
								<?=DatePicker::widget([
								    'model' => $dates,
								    'attribute' => 'date1',
								    'attribute2' => 'date2',
								    'options' => ['placeholder' => Yii::t('app', 'От даты')],
								    'options2' => ['placeholder' => Yii::t('app', 'До даты')],
								    'type' => DatePicker::TYPE_RANGE,
								    'form' => $form,
									'separator' => Yii::t('app', 'до'),
								    'pluginOptions' => [
								    	'todayHighlight' => true,
										'todayBtn' => true,
										'format' => 'dd.mm.yyyy',
										'autoclose' => true,
								    ]
								]);?>
							</div>
						</div>
						
						<div class="col-sm-2">
							<button type="submit" class="btn btn-success btn-block"><?=Yii::t('app', 'Фильтр')?></button>
						</div>
					</div>
					
					
					
					
				<?php ActiveForm::end(); ?>
			</div>
			
		</div>
		
		
		<div class="panel">
			<h3>Отчет. Заявки поступившие на сайт «E-region  мобильное приложение»</h3>
			<span>за период с <?=$dates->date1?> по <?=$dates->date2?></span> <br /> <br />
			
			<div>Поступило и обработано: <?=$stats['total']?></div> <br />
			<div>Находятся в статусе "Проверяется",  "В работе",  "В заморозке" : <?=$stats['processing']?></div> <br />
			<div>Находятся в статусе "Выполнено" : <?=$stats['processed']?></div> <br />
			<div>Находятся в статусе "Удалено" : <?=$stats['deleted']?></div> <br />
						
			<div>Из них создано по Автору: </div> <br />
			
			<?php foreach($stats['organizations'] as $type_name => $type_val) : ?>
				
				- <?=$type_name?> : <?=$type_val?> <br />
				
			<?php endforeach ?> <br /> <br />
			
			<table class="table">
				<thead>
					<tr>
						<td>#</td>
						<td>№ заявки</td>
						<td>Дата создания</td>
						<td>Статус</td>
						<td>Описание</td>
						<td>Действия по исполнению</td>
						<td>Исполнитель</td>
					</tr>
				</thead>
				<tbody>
					<?php $i_counter = 1; ?>
					<?php foreach($applications as $application) : ?>
						<tr>
							<td><?=$i_counter?></td>
							<td><?=$application->id?></td>
							<td><?=$application->date_created?></td>
							<td><?=$application->statusName?></td>
							<td><?=$application->description?></td>
							<td><?=$application->getCommendsRaw()?></td>
							<td><?=$application->getOrganizationNames()?></td>
						</tr>
						<?php $i_counter ++; ?>
					<?php endforeach ?>
					
				</tbody>
			</table>
			
			
		</div>
		
	</div>
	
</div>

