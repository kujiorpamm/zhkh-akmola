var chart1,chart2, chart3, chart4, mymap, is_mobile, prev_is_mobile;


$(window).resize(function(e){
	prev_is_mobile = is_mobile;
	is_mobile = $(window).width() <= 768;
	if(prev_is_mobile != is_mobile) {
		ajaxChart2();
		ajaxChart4();
	}
});

$(document).ready(function(){

	is_mobile = $(window).width() <= 768;

	moment.locale('ru');
	ajaxGeneral();
	ajaxChart1();
	ajaxChart2();
	ajaxChart3();
	ajaxChart4();
	ajaxListView();


	$("#filter").click(function(){
		ajaxGeneral();
		ajaxChart1();
		ajaxChart2();
		ajaxChart3();
		ajaxChart4();
		ajaxListView();
	});

	$('#pdf').click(function(){
		$("input[name='img1']").val(chart1.toBase64Image());
		$("input[name='img2']").val(chart2.toBase64Image());
		$("input[name='img3']").val(chart3.toBase64Image());
		$("input[name='img4']").val(chart4.toBase64Image());
		$("input[name='val1']").val($('#val-1').html());
		$("input[name='val2']").val($('#val-2').html());
		$("input[name='val3']").val($('#val-3').html());
		$("input[name='val4']").val($('#val-4').html());
		$("input[name='val5']").val($('#val-5').html());
		$("input[name='val6']").val($('#val-6').html());
		$("input[name='val7']").val($('#val-7').html());
		$("input[name='val8']").val($('#val-8').html());
		$("#form-print").submit();
	});


});

function ajaxGeneral() {
	$.ajax({
		async: false,
		url: '/statistics/default/general-data',
		type: 'POST',
		data: {
			date1: moment($("#applicationsearch-date1").val(), "DD.MM.YYYY").format("YYYY-MM-DD"),
			date2: moment($("#applicationsearch-date2").val(), "DD.MM.YYYY").format("YYYY-MM-DD"),
			type: $('#applicationsearch-application_type').val(),
			addr: $('#applicationsearch-address').val()

		},
		dataType: 'JSON',
		success: function(data) {
			$('#val-1').html(data['total']);
			$('#val-2').html(data['solved']);
			$('#val-3').html(data['day']);
			$('#val-4').html(data['month']);
			$('#val-5').html(data['types']);
			$('#val-6').html(data['dists']);
			$('#val-7').html(moment(data['peak_day']).format('LL') + " / " + moment(data['peak_month']).format('MMM YYYY'));
			$('#val-8').html(data['organization']);
		}
	});
}

function ajaxChart1() {
	$.ajax({
		async: false,
		url: '/statistics/default/ajax-chart-1',
		type: 'POST',
		dataType: 'JSON',
		data: {
			date1: moment($("#applicationsearch-date1").val(), "DD.MM.YYYY").format("YYYY-MM-DD"),
			date2: moment($("#applicationsearch-date2").val(), "DD.MM.YYYY").add(1, 'day').format("YYYY-MM-DD"),
			type: $('#applicationsearch-application_type').val(),
			addr: $('#applicationsearch-address').val()
		},
		success: function(data) {
			if(typeof chart1 !== 'undefined') chart1.destroy();
			chart1 = new Chart($("#chart1"), data);
		}
	});
}


function ajaxChart2() {
	$.ajax({
		url: '/statistics/default/ajax-chart-2',
		type: 'POST',
		dataType: 'JSON',
		data: {
			is_mobile: is_mobile,
			date1: moment($("#applicationsearch-date1").val(), "DD.MM.YYYY").format("YYYY-MM-DD"),
			date2: moment($("#applicationsearch-date2").val(), "DD.MM.YYYY").format("YYYY-MM-DD"),
			type: $('#applicationsearch-application_type').val(),
			addr: $('#applicationsearch-address').val()
		},
		success: function(data) {
			if(typeof chart2 !== 'undefined') chart2.destroy();
			chart2 = new Chart($("#chart2"), data);
			chart2.chart.ctx.canvas.removeEventListener('wheel', chart2.zoom._wheelHandler);
		}
	});
}

function ajaxChart3() {
	$.ajax({
		url: '/statistics/default/ajax-chart-3',
		type: 'POST',
		dataType: 'JSON',
		data: {
			date1: moment($("#applicationsearch-date1").val(), "DD.MM.YYYY").format("YYYY-MM-DD"),
			date2: moment($("#applicationsearch-date2").val(), "DD.MM.YYYY").format("YYYY-MM-DD"),
			type: $('#applicationsearch-application_type').val(),
			addr: $('#applicationsearch-address').val()
		},
		success: function(data) {
			if(typeof chart3 !== 'undefined') chart3.destroy();
			chart3 = new Chart($("#chart3"), data);
			chart3.chart.ctx.canvas.removeEventListener('wheel', chart3.zoom._wheelHandler);
		}
	});
}

function ajaxChart4() {
	$.ajax({
		url: '/statistics/default/ajax-chart-4',
		type: 'POST',
		dataType: 'JSON',
		data: {
			is_mobile: is_mobile,
			date1: moment($("#applicationsearch-date1").val(), "DD.MM.YYYY").format("YYYY-MM-DD"),
			date2: moment($("#applicationsearch-date2").val(), "DD.MM.YYYY").format("YYYY-MM-DD"),
			type: $('#applicationsearch-application_type').val(),
			addr: $('#applicationsearch-address').val()
		},
		success: function(data) {
			if(typeof chart4 !== 'undefined') chart4.destroy();
			chart4 = new Chart($("#chart4"), data);
			chart4.chart.ctx.canvas.removeEventListener('wheel', chart4.zoom._wheelHandler);
		}
	});
}

function ajaxListView() {
	$.ajax({
		url: '/statistics/default/ajax-list-view',
		type: 'POST',
		data: {
			date1: moment($("#applicationsearch-date1").val(), "DD.MM.YYYY").format("YYYY-MM-DD"),
			date2: moment($("#applicationsearch-date2").val(), "DD.MM.YYYY").format("YYYY-MM-DD"),
			type: $('#applicationsearch-application_type').val(),
			addr: $('#applicationsearch-address').val()
		},
		success: function(data) {
			$("#list_view").html(data);
		}
	});
}
