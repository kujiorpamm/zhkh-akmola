<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class EventMapNewAsset extends AppAsset
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
    public $css = [
    	'css/perfect-scrollbar.css',
        'https://api.tiles.mapbox.com/mapbox-gl-js/v0.49.0/mapbox-gl.css',
    ];
    public $js = [
        'https://api.tiles.mapbox.com/mapbox-gl-js/v0.49.0/mapbox-gl.js',
    	// 'js/eventmap/scriptnew.js',
    	'js/js.cookie.js',
    	'js/perfect-scrollbar.jquery.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\jui\JuiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
