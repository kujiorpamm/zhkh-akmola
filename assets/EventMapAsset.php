<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class EventMapAsset extends AppAsset
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
    public $css = [
    	'css/perfect-scrollbar.css',
        'https://api.tiles.mapbox.com/mapbox-gl-js/v0.49.0/mapbox-gl.css',
    ];
    public $js = [
    	'js/eventmap/script.js',
    	'js/js.cookie.js',
    	'js/perfect-scrollbar.jquery.js',
		'https://api-maps.yandex.ru/2.1.oldie.1/?lang=ru_RU&load=Map,Placemark,GeoObjectCollection,geoObject.addon.balloon',
    	'https://api.tiles.mapbox.com/mapbox-gl-js/v0.49.0/mapbox-gl.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\jui\JuiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
