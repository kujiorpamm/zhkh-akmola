<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class DashboardAsset extends AppAsset
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    	'vendor/dashboard/css/styles_d.css',
    	'vendor/dashboard/css/jsmeter_d.css',
    	'vendor/dashboard/css/tab.css',
    	'vendor/dashboard/css/tabs_d.css',
    	'vendor/dashboard/css/tabstyles.css',
    ];
    public $js = [
    	'vendor/dashboard/js/countUp.js',
    	'vendor/dashboard/js/jsmeter-1.1.2.min.js',
    	'vendor/dashboard/js/jsmeter.js',
    	'vendor/dashboard/js/bubles.js',
    	'vendor/dashboard/js/counts.js',
    	'vendor/dashboard/js/cbpFWTabs.js',
    	'vendor/dashboard/js/modernizr.custom.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset',
        'yii\jui\JuiAsset',
        'yii\bootstrap\BootstrapAsset',
    ]; 
}
