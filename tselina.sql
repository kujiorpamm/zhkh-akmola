-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Янв 22 2019 г., 09:50
-- Версия сервера: 10.1.28-MariaDB
-- Версия PHP: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `tselina`
--

-- --------------------------------------------------------

--
-- Структура таблицы `map_objects`
--

CREATE TABLE `map_objects` (
  `id` int(11) NOT NULL,
  `coords` varchar(128) NOT NULL,
  `type_id` int(11) NOT NULL,
  `data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `map_objects`
--

INSERT INTO `map_objects` (`id`, `coords`, `type_id`, `data`) VALUES
(4, '70.97333239139664,51.06380154198146', 1, '<p><strong>ГКП на ПХВ</strong>\r\n</p><p>«Целиноградская коммунальная служба» <br>с. Акмол, район промзона<br>строение 7. Офис: мкр Бахыт 12<span class=\"redactor-invisible-space\"></span>\r\n</p><p><span class=\"redactor-invisible-space\"></span><strong>Чмутов Владимир Иванович </strong><br>8 701 6239972\r\n</p><p>Запас топлива 2200 тонн<br></p>'),
(5, '70.97980877470638,51.06972135747233', 3, '<p><strong></strong><strong>ГКП «Целиноград Су Арнасы» </strong>\r\n</p>\r\n<p>с. Акмол ул . Жет Жаргы 1\r\n</p>\r\n<p><strong>Кулишев Женисбек Маратович</strong>\r\n</p>\r\n<p>8 702 129 90 48\r\n</p>'),
(6, '70.98415194396406,51.06052376939414', 4, '<p><strong></strong><strong>АО \"Акмолинская распределительная электросетевая компания\"</strong></p><p>телефон приемной 8 (71651) 91 012</p><p><strong>Герасименко Сергей Владимирович</strong></p><p>8701 599 85 39</p>'),
(7, '70.97194862384347,51.07586291487959', 6, '<p><strong></strong><strong>Средняя школа №5</strong> с. Акмол. ул Гагарина 1 раб тел: 30 309</p><p><strong>Яхина Алла Айдарбековна </strong></p><p>директор 8747 918 60 05</p>'),
(8, '70.9731560466274,51.06954961647446', 6, '<p><strong></strong><strong>ГККП «Детский сад «Жулдыз»</strong> мкр «Бахыт» 51-655</p><p><strong>Баранова Ольга Анатольевна <br></strong>заведующая 8 701 670 26 12</p><p><strong>Дежурный телефон </strong>51-657</p>'),
(9, '70.97804121195159,51.065257394397406', 5, '<p><strong></strong><strong>ГККП «Целиноградская районная поликлиника» <br></strong>мкр. «Бахыт», раб тел 51 553</p><p><strong>Досмырзаева Гульназ Толеновна </strong><br>главный врач 8 701 544 17 27</p>'),
(10, '70.96100097531274,51.07249309031809', 7, '<p><strong></strong><strong>Расчистка автодорог ГКП «Косшы Куат» <br></strong>мкр Черемушки раб тел 30 062</p><p><strong>Муканов Даурен Сайранович</strong><br>директор 8 705 364 45 22</p>'),
(11, '70.95833099199359,51.0658256507314', 8, '<p><strong></strong><strong>ТОО «Taza Zhol Group» <br></strong>раб тел 8 747 704 52 91</p><p><strong>Дуйсебеков Канат Муратович</strong><br>директор 8 701 557 36 73</p><p>График дежурства ежедневно с 9.00 до 18.00 часов, <br>раб тел 8 747 704 52 91</p>'),
(12, '70.96723005507678,51.0777392229632', 9, '<p><strong></strong><strong>Аким сельского округа Акмол <br></strong>Дуйсекеев Жанат Тулеуович <br>сот.87014196074</p><p><strong>Заместитель акима </strong><br>Биркенов Самат Газизович сот.87075365938</p><p>Адрес: село Акмол, улица Гагарина 15а, тел.871651-30560</p>');

-- --------------------------------------------------------

--
-- Структура таблицы `map_types`
--

CREATE TABLE `map_types` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `icon` varchar(512) NOT NULL,
  `sort` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `map_types`
--

INSERT INTO `map_types` (`id`, `name`, `icon`, `sort`) VALUES
(1, 'Теплоснабжение', '/images/icons-map/5c46cbe27659d.png', 0),
(3, 'Водоснабжение', '/images/icons-map/5c46cc2fcf17e.png', 0),
(4, 'Электроснабжение', '/images/icons-map/5c46cc3e14933.png', NULL),
(5, 'Здравоохранение', '/images/icons-map/5c46cc4b87e38.png', NULL),
(6, 'Образование', '/images/icons-map/5c46cc5bc5992.png', NULL),
(7, 'Дороги', '/images/icons-map/5c46cc7064ad1.png', NULL),
(8, 'Подрядные организации', '/images/icons-map/5c46cc81c1edc.png', NULL),
(9, 'Руководство', '/images/icons-map/5c46cc98eb2e6.png', NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `map_objects`
--
ALTER TABLE `map_objects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type_id` (`type_id`);

--
-- Индексы таблицы `map_types`
--
ALTER TABLE `map_types`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `map_objects`
--
ALTER TABLE `map_objects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `map_types`
--
ALTER TABLE `map_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `map_objects`
--
ALTER TABLE `map_objects`
  ADD CONSTRAINT `map_objects_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `map_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
