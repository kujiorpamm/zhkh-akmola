<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use app\models\SourceMessage;
use yii\data\ActiveDataProvider;


class SourceMessageSearch extends SourceMessage
{   

    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['category', 'message'], 'safe'],
        ];
    }
    
    public function scenarios()
    {
        return Model::scenarios();
    }
    
    public function search($params)
    {
        $query = SourceMessage::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            '{{%i18n_source_message}}.id' => $this->id,
        ]);

        $query->andFilterWhere(['like', '{{%source_message}}.category', $this->category])
            ->andFilterWhere(['like', '{{%source_message}}.message', $this->message]);

        return $dataProvider;
    }
    
}
