<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class DatesForm extends Model
{
    public $date1;
    public $date2;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['date1', 'date2'], 'required']
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'date1' => Yii::t('app', 'От'),
            'date2' => Yii::t('app', 'До'),
        ];
    }
    
    public function __construct($config = []) {
		parent::__construct($config);
		
		$this->date1 = date('01.m.Y', strtotime(date('Y-m-d')));
		$this->date2 = date('t.m.Y', strtotime(date('Y-m-d')));
		
	}
    
}
