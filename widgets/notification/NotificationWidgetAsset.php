<?php

namespace widgets\notification;

use yii\web\AssetBundle;

class NotificationWidgetAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    
    public $css = [

        'css/notification.less'
    ];

    public $depends = [
        // 'yii\web\JqueryAsset'
    ];

    public function init()
    {
        // Tell AssetBundle where the assets files are
        parent::init();
    }
}
