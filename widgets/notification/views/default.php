<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;

\widgets\notification\NotificationWidgetAsset::register($this);


?>



<div class="wgt_notification" onclick="$('#widget_not').modal('show');">

</div>

<?php Modal::begin([
    'id' => 'widget_not',
    'header' => Yii::t('app', 'Актуальные события'),
    'clientOptions' => [
        // 'show' => true
    ]
]) ?>

    <div class="widget_notifications">
        <?php foreach ($models as $model): ?>
            <div class="item">
                <div class="datetime">
                    <?=Yii::$app->formatter->asDate($model->datetime_from, 'php:d.m.Y H:i')?>
                    -
                    <?=Yii::$app->formatter->asDate($model->datetime_to, 'php:d.m.Y H:i')?>
                </div>
                <div class="message"><?=$model->message?></div>
            </div>
        <?php endforeach;?>
    </div>

<?php Modal::end();?>
