<?php

namespace app\widgets\mapbox\assets;

use yii\web\AssetBundle;

class MapboxAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
    public $css = [
        'https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.css',
    ];
    public $js = [
    	'https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\jui\JuiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
