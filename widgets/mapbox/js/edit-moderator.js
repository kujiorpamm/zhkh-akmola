
map.on('click', function(e) {
    $('#application-coords').val(e.lngLat.lat + ',' + e.lngLat.lng);
    var coordinates = e.lngLat;
    console.log(coordinates);
    new mapboxgl.Popup()
            .setLngLat(coordinates)
            .setHTML("Тут!")
            .addTo(map);
});

map.on('load', function () {

    // console.log([app_coords[1], app_coords[0]]);
    app_coords = [app_coords[1], app_coords[0]];

    new mapboxgl.Popup()
            .setLngLat(app_coords)
            .setHTML("Тут!")
            .addTo(map);

    map.flyTo({
        center: app_coords
    });

});
