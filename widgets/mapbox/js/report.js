
map.on('click', function(e) {
    $('#application-coords').val(e.lngLat.lat + ',' + e.lngLat.lng);
    var coordinates = e.lngLat;
    console.log(coordinates);
    new mapboxgl.Popup()
            .setLngLat(coordinates)
            .setHTML("Тут!")
            .addTo(map);
});

$(document).ready(function(){

	$('#app_main_cat').change(function(e) {
		main_category_change($(this).val());
	});

	$('#app_form').on('afterValidate', function (event, messages) {
	    if(typeof $('.has-error').first().offset() !== 'undefined') {
	        $('html, body').animate({
	            scrollTop: $('.has-error').first().offset().top - 150
	        }, 1000);
	    }
	});

});

function main_category_change(category) {
	var new_cat = category;
	$.ajax({
		url: "/ajax/application-subcategories",
		type: "POST",
		dataType: "JSON",
		data: {
			id: new_cat
		},
		complete: function(data) {

			var result = $.parseJSON(data.responseText);
			var new_data = '<option value>Выберите подкатегорию </option>';;
			$.each(result, function(k, v) {
				new_data += '<option value='+k+'>'+v+'</option>';
			});

			$('#application-application_type').html(new_data);
		},
		error: function(data) {
			$('#application-application_type').html("");
		},
		beforeSend: function() {
			$('#application-application_type').html("");
			$('#application-application_type').val("");
		}
	});

}
