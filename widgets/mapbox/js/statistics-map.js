map.on('load', function () {

    map.setLayoutProperty("district-boundary-7i4oqm", 'visibility', 'none');

    map.addSource('earthquakes', {
"type": "geojson",
"data": "/statistics/default/get-heatmap"
});

map.addLayer({
    "id": "earthquakes-heat",
    "type": "heatmap",
    "source": "earthquakes",
    "maxzoom": 14,
    "paint": {
    // Increase the heatmap weight based on frequency and property magnitude
    "heatmap-weight": [
        "interpolate",
        ["linear"],
        ["get", "mag"],
        0, 0,
        6, 1
    ],
    // Increase the heatmap color weight weight by zoom level
    // heatmap-intensity is a multiplier on top of heatmap-weight
    "heatmap-intensity": [
        "interpolate",
        ["linear"],
        ["zoom"],
        0, 1,
        9, 4
    ],
    // Color ramp for heatmap.  Domain is 0 (low) to 1 (high).
    // Begin color ramp at 0-stop with a 0-transparancy color
    // to create a blur-like effect.
    "heatmap-color": [
        "interpolate",
        ["linear"],
        ["heatmap-density"],
        0, "rgba(178,24,43,0)",
        1, "rgb(178,24,43)"
    ],
    // Adjust the heatmap radius by zoom level
    "heatmap-radius": [
        "interpolate",
        ["linear"],
        ["zoom"],
        0, 2,
        9, 20
    ],
    // Transition from heatmap to circle layer by zoom level
    "heatmap-opacity": [
        "interpolate",
        ["linear"],
        ["zoom"],
        7, 1,
        16, 0
    ],
    }
    });
})
